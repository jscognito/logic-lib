import { Injectable, NgModule, defineInjectable, inject } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import { HttpClient } from '@angular/common/http';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class LogicLibService {
    constructor() { }
}
LogicLibService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
LogicLibService.ctorParameters = () => [];
/** @nocollapse */ LogicLibService.ngInjectableDef = defineInjectable({ factory: function LogicLibService_Factory() { return new LogicLibService(); }, token: LogicLibService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class LogicLibModule {
}
LogicLibModule.decorators = [
    { type: NgModule, args: [{
                imports: []
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ConfigService {
    constructor() {
        this.config = {
            token: {
                type: null,
                value: null
            },
            channel: {
                clientId: null,
                grantType: null,
                secret: null,
            }
        };
    }
}
ConfigService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
ConfigService.ctorParameters = () => [];
/** @nocollapse */ ConfigService.ngInjectableDef = defineInjectable({ factory: function ConfigService_Factory() { return new ConfigService(); }, token: ConfigService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class SessionService {
    constructor() {
        this.session = {
            username: null
        };
    }
}
SessionService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
SessionService.ctorParameters = () => [];
/** @nocollapse */ SessionService.ngInjectableDef = defineInjectable({ factory: function SessionService_Factory() { return new SessionService(); }, token: SessionService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class InterceptorService {
    /**
     * @param {?} configService
     * @param {?} sessionService
     */
    constructor(configService, sessionService) {
        this.configService = configService;
        this.sessionService = sessionService;
    }
    /**
     * Método interceptor
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    intercept(request, next) {
        /** @type {?} */
        let authorization;
        /** @type {?} */
        let contentType;
        if (this.configService.config.token.value === null) {
            authorization = 'Basic' + ' ' + this.configService.config.channel.secret;
            contentType = 'application/x-www-form-urlencoded';
        }
        else {
            authorization = this.configService.config.token.type + ' ' + this.configService.config.token.value;
            contentType = 'application/json';
        }
        request = request.clone({
            setHeaders: {
                'Authorization': '' + authorization,
                'Content-Type': '' + contentType,
                'Audit': this.sessionService.session.username === null ? '' : '' + this.sessionService.session.username
            }
        });
        return next.handle(request)
            .catch(error => {
            {
                return this.handle400Error(error);
            }
        });
    }
    /**
     * @param {?} error
     * @return {?}
     */
    handle400Error(error) {
        try {
            return Observable.throw(error);
        }
        catch (e) {
            return Observable.throw(error);
        }
    }
}
InterceptorService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
InterceptorService.ctorParameters = () => [
    { type: ConfigService },
    { type: SessionService }
];
/** @nocollapse */ InterceptorService.ngInjectableDef = defineInjectable({ factory: function InterceptorService_Factory() { return new InterceptorService(inject(ConfigService), inject(SessionService)); }, token: InterceptorService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class HttpRequestService {
    /**
     * @param {?} httpClient
     */
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    /**
     * @param {?} typeRequest
     * @param {?} url
     * @param {?} bodyRequest
     * @param {?} queryString
     * @return {?}
     */
    httpRequest(typeRequest, url, bodyRequest, queryString) {
        return this.httpClient.request(typeRequest, url, { body: bodyRequest, observe: 'response', params: queryString });
    }
}
HttpRequestService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
HttpRequestService.ctorParameters = () => [
    { type: HttpClient }
];
/** @nocollapse */ HttpRequestService.ngInjectableDef = defineInjectable({ factory: function HttpRequestService_Factory() { return new HttpRequestService(inject(HttpClient)); }, token: HttpRequestService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class LoginService {
    /**
     * @param {?} httpRequestService
     */
    constructor(httpRequestService) {
        this.httpRequestService = httpRequestService;
    }
    /**
     * @param {?} bodyRequest
     * @return {?}
     */
    login(bodyRequest) {
        /** @type {?} */
        const type = 'POST';
        /** @type {?} */
        const url = this.API_LOGIN;
        /** @type {?} */
        const body = bodyRequest;
        /** @type {?} */
        const httpParams = null;
        return this.httpRequestService.httpRequest(type, url, body, httpParams);
    }
    /**
     * @return {?}
     */
    getApiLogin() {
        return this.API_LOGIN;
    }
    /**
     * @param {?} API_LOGIN
     * @return {?}
     */
    setApiLogin(API_LOGIN) {
        this.API_LOGIN = API_LOGIN;
    }
}
LoginService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
LoginService.ctorParameters = () => [
    { type: HttpRequestService }
];
/** @nocollapse */ LoginService.ngInjectableDef = defineInjectable({ factory: function LoginService_Factory() { return new LoginService(inject(HttpRequestService)); }, token: LoginService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class LogoutService {
    /**
     * @param {?} httpRequestService
     */
    constructor(httpRequestService) {
        this.httpRequestService = httpRequestService;
    }
    /**
     * @return {?}
     */
    logout() {
        /** @type {?} */
        const type = 'POST';
        /** @type {?} */
        const url = this.API_LOGOUT;
        /** @type {?} */
        const body = null;
        /** @type {?} */
        const httpParams = null;
        return this.httpRequestService.httpRequest(type, url, body, httpParams);
    }
    /**
     * @return {?}
     */
    getApiLogout() {
        return this.API_LOGOUT;
    }
    /**
     * @param {?} API_LOGIN
     * @return {?}
     */
    setApiLogout(API_LOGIN) {
        this.API_LOGOUT = API_LOGIN;
    }
}
LogoutService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
LogoutService.ctorParameters = () => [
    { type: HttpRequestService }
];
/** @nocollapse */ LogoutService.ngInjectableDef = defineInjectable({ factory: function LogoutService_Factory() { return new LogoutService(inject(HttpRequestService)); }, token: LogoutService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UserService$$1 {
    /**
     * @param {?} httpRequestService
     * @param {?} loginService
     * @param {?} configService
     */
    constructor(httpRequestService, loginService, configService) {
        this.httpRequestService = httpRequestService;
        this.loginService = loginService;
        this.configService = configService;
    }
    /**
     * @return {?}
     */
    login() {
        this.configService.config.channel.clientId = 'banistmoCNB';
        this.configService.config.channel.secret = 'YmFuaXN0bW9DTkI6c2VjcmV0QmFuaXN0bW9DTkI=';
        /** @type {?} */
        const bodyRequest = 'client_id=' + this.configService.config.channel.clientId + '&username=cristian.men&password=Colombia20.&grant_type=password';
        /** @type {?} */
        const type = 'POST';
        /** @type {?} */
        const url = this.API_LOGIN;
        /** @type {?} */
        const body = bodyRequest;
        /** @type {?} */
        const httpParams = null;
        return this.httpRequestService.httpRequest(type, url, body, httpParams);
    }
    /**
     * @param {?} username
     * @return {?}
     */
    veryfyUser(username) {
        /** @type {?} */
        const type = 'GET';
        /** @type {?} */
        const url = this.API_USER;
        /** @type {?} */
        const body = null;
        /** @type {?} */
        const httpParams = null;
        return this.httpRequestService.httpRequest(type, url, body, httpParams);
    }
    /**
     * @return {?}
     */
    getApiUser() {
        return this.API_USER;
    }
    /**
     * @param {?} API_USER
     * @return {?}
     */
    setApiUser(API_USER) {
        this.API_USER = API_USER;
    }
}
UserService$$1.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
UserService$$1.ctorParameters = () => [
    { type: HttpRequestService },
    { type: LoginService },
    { type: ConfigService }
];
/** @nocollapse */ UserService$$1.ngInjectableDef = defineInjectable({ factory: function UserService_Factory() { return new UserService$$1(inject(HttpRequestService), inject(LoginService), inject(ConfigService)); }, token: UserService$$1, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { InterceptorService, LoginService, LogoutService, ConfigService, SessionService, UserService$$1 as UserService, LogicLibService, LogicLibModule, HttpRequestService as ɵa };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naWMtbGliLmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9sb2dpYy1saWIvbGliL2xvZ2ljLWxpYi5zZXJ2aWNlLnRzIiwibmc6Ly9sb2dpYy1saWIvbGliL2xvZ2ljLWxpYi5tb2R1bGUudHMiLCJuZzovL2xvZ2ljLWxpYi9saWIvc2VydmljZXMvY29uZmlnL2NvbmZpZy5zZXJ2aWNlLnRzIiwibmc6Ly9sb2dpYy1saWIvbGliL3NlcnZpY2VzL3Nlc3Npb24vc2Vzc2lvbi5zZXJ2aWNlLnRzIiwibmc6Ly9sb2dpYy1saWIvbGliL3NlcnZpY2VzL2ludGVyY2VwdG9yL2ludGVyY2VwdG9yLnNlcnZpY2UudHMiLCJuZzovL2xvZ2ljLWxpYi9saWIvc2VydmljZXMvaHR0cFJlcXVlc3QvaHR0cC1yZXF1ZXN0LnNlcnZpY2UudHMiLCJuZzovL2xvZ2ljLWxpYi9saWIvc2VydmljZXMvbG9naW4vbG9naW4uc2VydmljZS50cyIsIm5nOi8vbG9naWMtbGliL2xpYi9zZXJ2aWNlcy9sb2dvdXQvbG9nb3V0LnNlcnZpY2UudHMiLCJuZzovL2xvZ2ljLWxpYi9saWIvc2VydmljZXMvdXNlci91c2VyLnNlcnZpY2UudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBMb2dpY0xpYlNlcnZpY2Uge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG59XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgTG9naWNMaWJNb2R1bGUgeyB9XG4iLCJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIENvbmZpZ1NlcnZpY2Uge1xuXG4gIHB1YmxpYyBjb25maWcgPSB7XG4gICAgdG9rZW46IHtcbiAgICAgIHR5cGU6IG51bGwsXG4gICAgICB2YWx1ZTogbnVsbFxuICAgIH0sXG4gICAgY2hhbm5lbDoge1xuICAgICAgY2xpZW50SWQ6IG51bGwsXG4gICAgICBncmFudFR5cGU6IG51bGwsXG4gICAgICBzZWNyZXQ6IG51bGwsXG4gICAgfVxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG59XG4iLCJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFNlc3Npb25TZXJ2aWNlIHtcblxuICBwdWJsaWMgc2Vzc2lvbiA9IHtcbiAgICB1c2VybmFtZTogbnVsbFxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG59XG4iLCJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBJbnRlcmNlcHRvciB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwL3NyYy9pbnRlcmNlcHRvcic7XG5pbXBvcnQgeyBIdHRwUmVxdWVzdCwgSHR0cEhhbmRsZXIsIEh0dHBFdmVudCwgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgJ3J4anMvYWRkL29wZXJhdG9yL2RvJztcbmltcG9ydCAncnhqcy9hZGQvb3BlcmF0b3IvY2F0Y2gnO1xuaW1wb3J0ICdyeGpzL09ic2VydmFibGUnO1xuaW1wb3J0ICdyeGpzL2FkZC9vYnNlcnZhYmxlL3Rocm93JztcbmltcG9ydCB7IENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi9jb25maWcvY29uZmlnLnNlcnZpY2UnO1xuaW1wb3J0IHsgU2Vzc2lvblNlcnZpY2UgfSBmcm9tICcuLi9zZXNzaW9uL3Nlc3Npb24uc2VydmljZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIEludGVyY2VwdG9yU2VydmljZSBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XG5cblxuICBjb25zdHJ1Y3RvcihcbiAgICBwdWJsaWMgY29uZmlnU2VydmljZTogQ29uZmlnU2VydmljZSxcbiAgICBwdWJsaWMgc2Vzc2lvblNlcnZpY2U6IFNlc3Npb25TZXJ2aWNlICkge1xuICB9XG5cbiAgLyoqXG4gICAqIE3Dg8KpdG9kbyBpbnRlcmNlcHRvclxuICAgKiBAcGFyYW0gcmVxdWVzdCBcbiAgICogQHBhcmFtIG5leHQgXG4gICAqL1xuICBpbnRlcmNlcHQocmVxdWVzdDogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XG4gICAgbGV0IGF1dGhvcml6YXRpb247XG4gICAgbGV0IGNvbnRlbnRUeXBlO1xuICAgIFxuICAgIGlmICh0aGlzLmNvbmZpZ1NlcnZpY2UuY29uZmlnLnRva2VuLnZhbHVlID09PSBudWxsKSB7XG4gICAgICBhdXRob3JpemF0aW9uID0gJ0Jhc2ljJyArICcgJyArIHRoaXMuY29uZmlnU2VydmljZS5jb25maWcuY2hhbm5lbC5zZWNyZXQ7XG4gICAgICBjb250ZW50VHlwZSA9ICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnO1xuICAgIH0gZWxzZSB7XG4gICAgICBhdXRob3JpemF0aW9uID0gdGhpcy5jb25maWdTZXJ2aWNlLmNvbmZpZy50b2tlbi50eXBlICsgJyAnICsgdGhpcy5jb25maWdTZXJ2aWNlLmNvbmZpZy50b2tlbi52YWx1ZTtcbiAgICAgIGNvbnRlbnRUeXBlID0gJ2FwcGxpY2F0aW9uL2pzb24nO1xuICAgIH1cbiAgICBcbiAgICByZXF1ZXN0ID0gcmVxdWVzdC5jbG9uZSh7XG4gICAgICBzZXRIZWFkZXJzOiB7XG4gICAgICAgICdBdXRob3JpemF0aW9uJzonJysgYXV0aG9yaXphdGlvbixcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6JycrIGNvbnRlbnRUeXBlLFxuICAgICAgICAnQXVkaXQnOiB0aGlzLnNlc3Npb25TZXJ2aWNlLnNlc3Npb24udXNlcm5hbWUgPT09IG51bGwgPyAnJzonJyArIHRoaXMuc2Vzc2lvblNlcnZpY2Uuc2Vzc2lvbi51c2VybmFtZVxuICAgICAgfVxuICAgIH0pO1xuICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXF1ZXN0KVxuICAgICAgLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAge1xuICAgICAgICAgIHJldHVybiB0aGlzLmhhbmRsZTQwMEVycm9yKGVycm9yKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gIH1cbiBcbiAgaGFuZGxlNDAwRXJyb3IoZXJyb3IpIHtcbiAgICB0cnkge1xuICAgICAgcmV0dXJuIE9ic2VydmFibGUudGhyb3coZXJyb3IpO1xuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgIHJldHVybiBPYnNlcnZhYmxlLnRocm93KGVycm9yKTtcbiAgICB9XG5cbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycywgSHR0cFBhcmFtc30gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvT2JzZXJ2YWJsZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIEh0dHBSZXF1ZXN0U2VydmljZSB7XG5cbiAgY29uc3RydWN0b3IocHVibGljIGh0dHBDbGllbnQ6IEh0dHBDbGllbnQpIHsgfVxuXG4gIHB1YmxpYyBodHRwUmVxdWVzdCh0eXBlUmVxdWVzdDogc3RyaW5nLCB1cmw6IHN0cmluZywgYm9keVJlcXVlc3Q6IGFueSwgcXVlcnlTdHJpbmc6IEh0dHBQYXJhbXMpOiBPYnNlcnZhYmxlIDxhbnk+ICB7XG5cbiAgICBcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnJlcXVlc3QodHlwZVJlcXVlc3QsIHVybCwgeyBib2R5OiBib2R5UmVxdWVzdCwgb2JzZXJ2ZTogJ3Jlc3BvbnNlJywgcGFyYW1zOiBxdWVyeVN0cmluZyB9KTtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwUmVxdWVzdFNlcnZpY2UgfSBmcm9tICcuLi9odHRwUmVxdWVzdC9odHRwLXJlcXVlc3Quc2VydmljZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9pbnRlcm5hbC9PYnNlcnZhYmxlJztcblxuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBMb2dpblNlcnZpY2Uge1xuXG4gIHByaXZhdGUgQVBJX0xPR0lOOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IocHVibGljIGh0dHBSZXF1ZXN0U2VydmljZTogSHR0cFJlcXVlc3RTZXJ2aWNlKSB7IH1cblxuICBsb2dpbihib2R5UmVxdWVzdDogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICBjb25zdCB0eXBlID0gJ1BPU1QnO1xuICAgIGNvbnN0IHVybCA9IHRoaXMuQVBJX0xPR0lOO1xuICAgIGNvbnN0IGJvZHkgPSBib2R5UmVxdWVzdDtcbiAgICBjb25zdCBodHRwUGFyYW1zID0gbnVsbDtcbiAgICByZXR1cm4gdGhpcy5odHRwUmVxdWVzdFNlcnZpY2UuaHR0cFJlcXVlc3QodHlwZSwgdXJsLCBib2R5LCBodHRwUGFyYW1zKTtcbiAgfVxuXG4gIGdldEFwaUxvZ2luKCkge1xuICAgIHJldHVybiB0aGlzLkFQSV9MT0dJTjtcbiAgfVxuXG4gIHNldEFwaUxvZ2luKEFQSV9MT0dJTjogc3RyaW5nKSB7XG4gICAgdGhpcy5BUElfTE9HSU4gPSBBUElfTE9HSU47XG4gIH1cbn1cbiIsImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBSZXF1ZXN0U2VydmljZSB9IGZyb20gJy4uL2h0dHBSZXF1ZXN0L2h0dHAtcmVxdWVzdC5zZXJ2aWNlJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL2ludGVybmFsL09ic2VydmFibGUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBMb2dvdXRTZXJ2aWNlIHtcblxuICBwcml2YXRlIEFQSV9MT0dPVVQ6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihwdWJsaWMgaHR0cFJlcXVlc3RTZXJ2aWNlOiBIdHRwUmVxdWVzdFNlcnZpY2UpIHsgfVxuXG4gIGxvZ291dCgpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIGNvbnN0IHR5cGUgPSAnUE9TVCc7XG4gICAgY29uc3QgdXJsID0gdGhpcy5BUElfTE9HT1VUO1xuICAgIGNvbnN0IGJvZHkgPSBudWxsO1xuICAgIGNvbnN0IGh0dHBQYXJhbXMgPSBudWxsO1xuICAgIHJldHVybiB0aGlzLmh0dHBSZXF1ZXN0U2VydmljZS5odHRwUmVxdWVzdCh0eXBlLCB1cmwsIGJvZHksIGh0dHBQYXJhbXMpO1xuICB9XG5cbiAgZ2V0QXBpTG9nb3V0KCkge1xuICAgIHJldHVybiB0aGlzLkFQSV9MT0dPVVQ7XG4gIH1cblxuICBzZXRBcGlMb2dvdXQoQVBJX0xPR0lOOiBzdHJpbmcpIHtcbiAgICB0aGlzLkFQSV9MT0dPVVQgPSBBUElfTE9HSU47XG4gIH1cbn1cbiIsImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBSZXF1ZXN0U2VydmljZSB9IGZyb20gJy4uL2h0dHBSZXF1ZXN0L2h0dHAtcmVxdWVzdC5zZXJ2aWNlJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL2ludGVybmFsL09ic2VydmFibGUnO1xuaW1wb3J0IHsgTG9naW5TZXJ2aWNlLCBDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vcHVibGljX2FwaSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFVzZXJTZXJ2aWNlIHtcblxuICBwcml2YXRlIEFQSV9MT0dJTjogc3RyaW5nO1xuICBwcml2YXRlIEFQSV9VU0VSOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHVibGljIGh0dHBSZXF1ZXN0U2VydmljZTogSHR0cFJlcXVlc3RTZXJ2aWNlLFxuICAgIHB1YmxpYyBsb2dpblNlcnZpY2U6IExvZ2luU2VydmljZSxcbiAgICBwdWJsaWMgY29uZmlnU2VydmljZTogQ29uZmlnU2VydmljZVxuICApIHsgfVxuXG4gIGxvZ2luKCkge1xuXG4gICAgdGhpcy5jb25maWdTZXJ2aWNlLmNvbmZpZy5jaGFubmVsLmNsaWVudElkID0gJ2JhbmlzdG1vQ05CJztcbiAgICB0aGlzLmNvbmZpZ1NlcnZpY2UuY29uZmlnLmNoYW5uZWwuc2VjcmV0ID0gJ1ltRnVhWE4wYlc5RFRrSTZjMlZqY21WMFFtRnVhWE4wYlc5RFRrST0nO1xuICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTptYXgtbGluZS1sZW5ndGhcbiAgICBjb25zdCBib2R5UmVxdWVzdDogYW55ID0gJ2NsaWVudF9pZD0nICsgdGhpcy5jb25maWdTZXJ2aWNlLmNvbmZpZy5jaGFubmVsLmNsaWVudElkICsgJyZ1c2VybmFtZT1jcmlzdGlhbi5tZW4mcGFzc3dvcmQ9Q29sb21iaWEyMC4mZ3JhbnRfdHlwZT1wYXNzd29yZCc7XG5cbiAgICBjb25zdCB0eXBlID0gJ1BPU1QnO1xuICAgIGNvbnN0IHVybCA9IHRoaXMuQVBJX0xPR0lOO1xuICAgIGNvbnN0IGJvZHkgPSBib2R5UmVxdWVzdDtcbiAgICBjb25zdCBodHRwUGFyYW1zID0gbnVsbDtcbiAgICByZXR1cm4gdGhpcy5odHRwUmVxdWVzdFNlcnZpY2UuaHR0cFJlcXVlc3QodHlwZSwgdXJsLCBib2R5LCBodHRwUGFyYW1zKTtcbiAgfVxuXG4gIHZlcnlmeVVzZXIodXNlcm5hbWU6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgY29uc3QgdHlwZSA9ICdHRVQnO1xuICAgIGNvbnN0IHVybCA9IHRoaXMuQVBJX1VTRVI7XG4gICAgY29uc3QgYm9keSA9IG51bGw7XG4gICAgY29uc3QgaHR0cFBhcmFtcyA9IG51bGw7XG4gICAgcmV0dXJuIHRoaXMuaHR0cFJlcXVlc3RTZXJ2aWNlLmh0dHBSZXF1ZXN0KHR5cGUsIHVybCwgYm9keSwgaHR0cFBhcmFtcyk7XG4gIH1cblxuICBnZXRBcGlVc2VyKCkge1xuICAgIHJldHVybiB0aGlzLkFQSV9VU0VSO1xuICB9XG5cbiAgc2V0QXBpVXNlcihBUElfVVNFUjogc3RyaW5nKSB7XG4gICAgdGhpcy5BUElfVVNFUiA9IEFQSV9VU0VSO1xuICB9XG59XG4iXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUE7SUFPRSxpQkFBaUI7OztZQUxsQixVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7Ozs7Ozs7Ozs7QUNKRDs7O1lBRUMsUUFBUSxTQUFDO2dCQUNSLE9BQU8sRUFBRSxFQUNSO2FBQ0Y7Ozs7Ozs7QUNMRDtJQW1CRTtzQkFaZ0I7WUFDZCxLQUFLLEVBQUU7Z0JBQ0wsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsS0FBSyxFQUFFLElBQUk7YUFDWjtZQUNELE9BQU8sRUFBRTtnQkFDUCxRQUFRLEVBQUUsSUFBSTtnQkFDZCxTQUFTLEVBQUUsSUFBSTtnQkFDZixNQUFNLEVBQUUsSUFBSTthQUNiO1NBQ0Y7S0FFZ0I7OztZQWpCbEIsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7Ozs7Ozs7O0FDSkQ7SUFXRTt1QkFKaUI7WUFDZixRQUFRLEVBQUUsSUFBSTtTQUNmO0tBRWdCOzs7WUFUbEIsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7Ozs7Ozs7O0FDSEQ7Ozs7O0lBaUJFLFlBQ1MsZUFDQTtRQURBLGtCQUFhLEdBQWIsYUFBYTtRQUNiLG1CQUFjLEdBQWQsY0FBYztLQUN0Qjs7Ozs7OztJQU9ELFNBQVMsQ0FBQyxPQUF5QixFQUFFLElBQWlCOztRQUNwRCxJQUFJLGFBQWEsQ0FBQzs7UUFDbEIsSUFBSSxXQUFXLENBQUM7UUFFaEIsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxLQUFLLElBQUksRUFBRTtZQUNsRCxhQUFhLEdBQUcsT0FBTyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDO1lBQ3pFLFdBQVcsR0FBRyxtQ0FBbUMsQ0FBQztTQUNuRDthQUFNO1lBQ0wsYUFBYSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7WUFDbkcsV0FBVyxHQUFHLGtCQUFrQixDQUFDO1NBQ2xDO1FBRUQsT0FBTyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUM7WUFDdEIsVUFBVSxFQUFFO2dCQUNWLGVBQWUsRUFBQyxFQUFFLEdBQUUsYUFBYTtnQkFDakMsY0FBYyxFQUFDLEVBQUUsR0FBRSxXQUFXO2dCQUM5QixPQUFPLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsUUFBUSxLQUFLLElBQUksR0FBRyxFQUFFLEdBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFFBQVE7YUFDdEc7U0FDRixDQUFDLENBQUM7UUFDSCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDO2FBQ3hCLEtBQUssQ0FBQyxLQUFLO1lBQ1Y7Z0JBQ0UsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ25DO1NBQ0YsQ0FBQyxDQUFDO0tBQ047Ozs7O0lBRUQsY0FBYyxDQUFDLEtBQUs7UUFDbEIsSUFBSTtZQUNGLE9BQU8sVUFBVSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNoQztRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1YsT0FBTyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2hDO0tBRUY7OztZQWxERixVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7Ozs7WUFMUSxhQUFhO1lBQ2IsY0FBYzs7Ozs7Ozs7QUNWdkI7Ozs7SUFTRSxZQUFtQixVQUFzQjtRQUF0QixlQUFVLEdBQVYsVUFBVSxDQUFZO0tBQUs7Ozs7Ozs7O0lBRXZDLFdBQVcsQ0FBQyxXQUFtQixFQUFFLEdBQVcsRUFBRSxXQUFnQixFQUFFLFdBQXVCO1FBRzVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLEdBQUcsRUFBRSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQzs7OztZQVZySCxVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7Ozs7WUFMUSxVQUFVOzs7Ozs7OztBQ0RuQjs7OztJQVlFLFlBQW1CLGtCQUFzQztRQUF0Qyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO0tBQUs7Ozs7O0lBRTlELEtBQUssQ0FBQyxXQUFnQjs7UUFDcEIsTUFBTSxJQUFJLEdBQUcsTUFBTSxDQUFDOztRQUNwQixNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDOztRQUMzQixNQUFNLElBQUksR0FBRyxXQUFXLENBQUM7O1FBQ3pCLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQztRQUN4QixPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUM7S0FDekU7Ozs7SUFFRCxXQUFXO1FBQ1QsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0tBQ3ZCOzs7OztJQUVELFdBQVcsQ0FBQyxTQUFpQjtRQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztLQUM1Qjs7O1lBdkJGLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7OztZQU5RLGtCQUFrQjs7Ozs7Ozs7QUNEM0I7Ozs7SUFXRSxZQUFtQixrQkFBc0M7UUFBdEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtLQUFLOzs7O0lBRTlELE1BQU07O1FBQ0osTUFBTSxJQUFJLEdBQUcsTUFBTSxDQUFDOztRQUNwQixNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDOztRQUM1QixNQUFNLElBQUksR0FBRyxJQUFJLENBQUM7O1FBQ2xCLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQztRQUN4QixPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUM7S0FDekU7Ozs7SUFFRCxZQUFZO1FBQ1YsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO0tBQ3hCOzs7OztJQUVELFlBQVksQ0FBQyxTQUFpQjtRQUM1QixJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQztLQUM3Qjs7O1lBdkJGLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7OztZQUxRLGtCQUFrQjs7Ozs7Ozs7QUNEM0I7Ozs7OztJQWFFLFlBQ1Msb0JBQ0EsY0FDQTtRQUZBLHVCQUFrQixHQUFsQixrQkFBa0I7UUFDbEIsaUJBQVksR0FBWixZQUFZO1FBQ1osa0JBQWEsR0FBYixhQUFhO0tBQ2pCOzs7O0lBRUwsS0FBSztRQUVILElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsYUFBYSxDQUFDO1FBQzNELElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsMENBQTBDLENBQUM7O1FBRXRGLE1BQU0sV0FBVyxHQUFRLFlBQVksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsUUFBUSxHQUFHLGlFQUFpRSxDQUFDOztRQUV2SixNQUFNLElBQUksR0FBRyxNQUFNLENBQUM7O1FBQ3BCLE1BQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7O1FBQzNCLE1BQU0sSUFBSSxHQUFHLFdBQVcsQ0FBQzs7UUFDekIsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxVQUFVLENBQUMsQ0FBQztLQUN6RTs7Ozs7SUFFRCxVQUFVLENBQUMsUUFBYTs7UUFDdEIsTUFBTSxJQUFJLEdBQUcsS0FBSyxDQUFDOztRQUNuQixNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDOztRQUMxQixNQUFNLElBQUksR0FBRyxJQUFJLENBQUM7O1FBQ2xCLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQztRQUN4QixPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUM7S0FDekU7Ozs7SUFFRCxVQUFVO1FBQ1IsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0tBQ3RCOzs7OztJQUVELFVBQVUsQ0FBQyxRQUFnQjtRQUN6QixJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztLQUMxQjs7O1lBMUNGLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7OztZQU5RLGtCQUFrQjtZQUVsQixZQUFZO1lBQUUsYUFBYTs7Ozs7Ozs7Ozs7Ozs7OzsifQ==