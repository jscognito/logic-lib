import { HttpRequestService } from '../httpRequest/http-request.service';
import { Observable } from 'rxjs/internal/Observable';
import { LoginService, ConfigService } from '../../../public_api';
export declare class UserService {
    httpRequestService: HttpRequestService;
    loginService: LoginService;
    configService: ConfigService;
    private API_LOGIN;
    private API_USER;
    constructor(httpRequestService: HttpRequestService, loginService: LoginService, configService: ConfigService);
    login(): Observable<any>;
    veryfyUser(username: any): Observable<any>;
    getApiUser(): string;
    setApiUser(API_USER: string): void;
}
