import { HttpRequestService } from '../httpRequest/http-request.service';
import { Observable } from 'rxjs/internal/Observable';
export declare class LogoutService {
    httpRequestService: HttpRequestService;
    private API_LOGOUT;
    constructor(httpRequestService: HttpRequestService);
    logout(): Observable<any>;
    getApiLogout(): string;
    setApiLogout(API_LOGIN: string): void;
}
