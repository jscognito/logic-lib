import { HttpRequestService } from '../httpRequest/http-request.service';
import { Observable } from 'rxjs/internal/Observable';
export declare class LoginService {
    httpRequestService: HttpRequestService;
    private API_LOGIN;
    constructor(httpRequestService: HttpRequestService);
    login(bodyRequest: any): Observable<any>;
    getApiLogin(): string;
    setApiLogin(API_LOGIN: string): void;
}
