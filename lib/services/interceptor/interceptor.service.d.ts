import { HttpInterceptor } from '@angular/common/http/src/interceptor';
import { HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import { ConfigService } from '../config/config.service';
import { SessionService } from '../session/session.service';
export declare class InterceptorService implements HttpInterceptor {
    configService: ConfigService;
    sessionService: SessionService;
    constructor(configService: ConfigService, sessionService: SessionService);
    /**
     * Método interceptor
     * @param request
     * @param next
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
    handle400Error(error: any): Observable<never>;
}
