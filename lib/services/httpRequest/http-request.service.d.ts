import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
export declare class HttpRequestService {
    httpClient: HttpClient;
    constructor(httpClient: HttpClient);
    httpRequest(typeRequest: string, url: string, bodyRequest: any, queryString: HttpParams): Observable<any>;
}
