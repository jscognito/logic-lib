/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class ConfigService {
    constructor() {
        this.config = {
            token: {
                type: null,
                value: null
            },
            channel: {
                clientId: null,
                grantType: null,
                secret: null,
            }
        };
    }
}
ConfigService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
ConfigService.ctorParameters = () => [];
/** @nocollapse */ ConfigService.ngInjectableDef = i0.defineInjectable({ factory: function ConfigService_Factory() { return new ConfigService(); }, token: ConfigService, providedIn: "root" });
function ConfigService_tsickle_Closure_declarations() {
    /** @type {?} */
    ConfigService.prototype.config;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9sb2dpYy1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvY29uZmlnL2NvbmZpZy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOztBQUszQyxNQUFNO0lBY0o7c0JBWmdCO1lBQ2QsS0FBSyxFQUFFO2dCQUNMLElBQUksRUFBRSxJQUFJO2dCQUNWLEtBQUssRUFBRSxJQUFJO2FBQ1o7WUFDRCxPQUFPLEVBQUU7Z0JBQ1AsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsU0FBUyxFQUFFLElBQUk7Z0JBQ2YsTUFBTSxFQUFFLElBQUk7YUFDYjtTQUNGO0tBRWdCOzs7WUFqQmxCLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgQ29uZmlnU2VydmljZSB7XG5cbiAgcHVibGljIGNvbmZpZyA9IHtcbiAgICB0b2tlbjoge1xuICAgICAgdHlwZTogbnVsbCxcbiAgICAgIHZhbHVlOiBudWxsXG4gICAgfSxcbiAgICBjaGFubmVsOiB7XG4gICAgICBjbGllbnRJZDogbnVsbCxcbiAgICAgIGdyYW50VHlwZTogbnVsbCxcbiAgICAgIHNlY3JldDogbnVsbCxcbiAgICB9XG4gIH07XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cbn1cbiJdfQ==