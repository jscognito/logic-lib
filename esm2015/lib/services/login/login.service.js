/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpRequestService } from '../httpRequest/http-request.service';
import * as i0 from "@angular/core";
import * as i1 from "../httpRequest/http-request.service";
export class LoginService {
    /**
     * @param {?} httpRequestService
     */
    constructor(httpRequestService) {
        this.httpRequestService = httpRequestService;
    }
    /**
     * @param {?} bodyRequest
     * @return {?}
     */
    login(bodyRequest) {
        /** @type {?} */
        const type = 'POST';
        /** @type {?} */
        const url = this.API_LOGIN;
        /** @type {?} */
        const body = bodyRequest;
        /** @type {?} */
        const httpParams = null;
        return this.httpRequestService.httpRequest(type, url, body, httpParams);
    }
    /**
     * @return {?}
     */
    getApiLogin() {
        return this.API_LOGIN;
    }
    /**
     * @param {?} API_LOGIN
     * @return {?}
     */
    setApiLogin(API_LOGIN) {
        this.API_LOGIN = API_LOGIN;
    }
}
LoginService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
LoginService.ctorParameters = () => [
    { type: HttpRequestService }
];
/** @nocollapse */ LoginService.ngInjectableDef = i0.defineInjectable({ factory: function LoginService_Factory() { return new LoginService(i0.inject(i1.HttpRequestService)); }, token: LoginService, providedIn: "root" });
function LoginService_tsickle_Closure_declarations() {
    /** @type {?} */
    LoginService.prototype.API_LOGIN;
    /** @type {?} */
    LoginService.prototype.httpRequestService;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2xvZ2ljLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlcy9sb2dpbi9sb2dpbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHFDQUFxQyxDQUFDOzs7QUFPekUsTUFBTTs7OztJQUlKLFlBQW1CLGtCQUFzQztRQUF0Qyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO0tBQUs7Ozs7O0lBRTlELEtBQUssQ0FBQyxXQUFnQjs7UUFDcEIsTUFBTSxJQUFJLEdBQUcsTUFBTSxDQUFDOztRQUNwQixNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDOztRQUMzQixNQUFNLElBQUksR0FBRyxXQUFXLENBQUM7O1FBQ3pCLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQztRQUN4QixNQUFNLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxVQUFVLENBQUMsQ0FBQztLQUN6RTs7OztJQUVELFdBQVc7UUFDVCxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztLQUN2Qjs7Ozs7SUFFRCxXQUFXLENBQUMsU0FBaUI7UUFDM0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7S0FDNUI7OztZQXZCRixVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7Ozs7WUFOUSxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwUmVxdWVzdFNlcnZpY2UgfSBmcm9tICcuLi9odHRwUmVxdWVzdC9odHRwLXJlcXVlc3Quc2VydmljZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9pbnRlcm5hbC9PYnNlcnZhYmxlJztcblxuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBMb2dpblNlcnZpY2Uge1xuXG4gIHByaXZhdGUgQVBJX0xPR0lOOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IocHVibGljIGh0dHBSZXF1ZXN0U2VydmljZTogSHR0cFJlcXVlc3RTZXJ2aWNlKSB7IH1cblxuICBsb2dpbihib2R5UmVxdWVzdDogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICBjb25zdCB0eXBlID0gJ1BPU1QnO1xuICAgIGNvbnN0IHVybCA9IHRoaXMuQVBJX0xPR0lOO1xuICAgIGNvbnN0IGJvZHkgPSBib2R5UmVxdWVzdDtcbiAgICBjb25zdCBodHRwUGFyYW1zID0gbnVsbDtcbiAgICByZXR1cm4gdGhpcy5odHRwUmVxdWVzdFNlcnZpY2UuaHR0cFJlcXVlc3QodHlwZSwgdXJsLCBib2R5LCBodHRwUGFyYW1zKTtcbiAgfVxuXG4gIGdldEFwaUxvZ2luKCkge1xuICAgIHJldHVybiB0aGlzLkFQSV9MT0dJTjtcbiAgfVxuXG4gIHNldEFwaUxvZ2luKEFQSV9MT0dJTjogc3RyaW5nKSB7XG4gICAgdGhpcy5BUElfTE9HSU4gPSBBUElfTE9HSU47XG4gIH1cbn1cbiJdfQ==