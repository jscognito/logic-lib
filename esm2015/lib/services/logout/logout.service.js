/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpRequestService } from '../httpRequest/http-request.service';
import * as i0 from "@angular/core";
import * as i1 from "../httpRequest/http-request.service";
export class LogoutService {
    /**
     * @param {?} httpRequestService
     */
    constructor(httpRequestService) {
        this.httpRequestService = httpRequestService;
    }
    /**
     * @return {?}
     */
    logout() {
        /** @type {?} */
        const type = 'POST';
        /** @type {?} */
        const url = this.API_LOGOUT;
        /** @type {?} */
        const body = null;
        /** @type {?} */
        const httpParams = null;
        return this.httpRequestService.httpRequest(type, url, body, httpParams);
    }
    /**
     * @return {?}
     */
    getApiLogout() {
        return this.API_LOGOUT;
    }
    /**
     * @param {?} API_LOGIN
     * @return {?}
     */
    setApiLogout(API_LOGIN) {
        this.API_LOGOUT = API_LOGIN;
    }
}
LogoutService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
LogoutService.ctorParameters = () => [
    { type: HttpRequestService }
];
/** @nocollapse */ LogoutService.ngInjectableDef = i0.defineInjectable({ factory: function LogoutService_Factory() { return new LogoutService(i0.inject(i1.HttpRequestService)); }, token: LogoutService, providedIn: "root" });
function LogoutService_tsickle_Closure_declarations() {
    /** @type {?} */
    LogoutService.prototype.API_LOGOUT;
    /** @type {?} */
    LogoutService.prototype.httpRequestService;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nb3V0LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9sb2dpYy1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvbG9nb3V0L2xvZ291dC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHFDQUFxQyxDQUFDOzs7QUFNekUsTUFBTTs7OztJQUlKLFlBQW1CLGtCQUFzQztRQUF0Qyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO0tBQUs7Ozs7SUFFOUQsTUFBTTs7UUFDSixNQUFNLElBQUksR0FBRyxNQUFNLENBQUM7O1FBQ3BCLE1BQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7O1FBQzVCLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQzs7UUFDbEIsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLE1BQU0sQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0tBQ3pFOzs7O0lBRUQsWUFBWTtRQUNWLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO0tBQ3hCOzs7OztJQUVELFlBQVksQ0FBQyxTQUFpQjtRQUM1QixJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQztLQUM3Qjs7O1lBdkJGLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7OztZQUxRLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBSZXF1ZXN0U2VydmljZSB9IGZyb20gJy4uL2h0dHBSZXF1ZXN0L2h0dHAtcmVxdWVzdC5zZXJ2aWNlJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL2ludGVybmFsL09ic2VydmFibGUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBMb2dvdXRTZXJ2aWNlIHtcblxuICBwcml2YXRlIEFQSV9MT0dPVVQ6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihwdWJsaWMgaHR0cFJlcXVlc3RTZXJ2aWNlOiBIdHRwUmVxdWVzdFNlcnZpY2UpIHsgfVxuXG4gIGxvZ291dCgpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIGNvbnN0IHR5cGUgPSAnUE9TVCc7XG4gICAgY29uc3QgdXJsID0gdGhpcy5BUElfTE9HT1VUO1xuICAgIGNvbnN0IGJvZHkgPSBudWxsO1xuICAgIGNvbnN0IGh0dHBQYXJhbXMgPSBudWxsO1xuICAgIHJldHVybiB0aGlzLmh0dHBSZXF1ZXN0U2VydmljZS5odHRwUmVxdWVzdCh0eXBlLCB1cmwsIGJvZHksIGh0dHBQYXJhbXMpO1xuICB9XG5cbiAgZ2V0QXBpTG9nb3V0KCkge1xuICAgIHJldHVybiB0aGlzLkFQSV9MT0dPVVQ7XG4gIH1cblxuICBzZXRBcGlMb2dvdXQoQVBJX0xPR0lOOiBzdHJpbmcpIHtcbiAgICB0aGlzLkFQSV9MT0dPVVQgPSBBUElfTE9HSU47XG4gIH1cbn1cbiJdfQ==