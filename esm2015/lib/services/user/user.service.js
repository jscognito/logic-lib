/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpRequestService } from '../httpRequest/http-request.service';
import { LoginService, ConfigService } from '../../../public_api';
import * as i0 from "@angular/core";
import * as i1 from "../httpRequest/http-request.service";
import * as i2 from "../login/login.service";
import * as i3 from "../config/config.service";
export class UserService {
    /**
     * @param {?} httpRequestService
     * @param {?} loginService
     * @param {?} configService
     */
    constructor(httpRequestService, loginService, configService) {
        this.httpRequestService = httpRequestService;
        this.loginService = loginService;
        this.configService = configService;
    }
    /**
     * @return {?}
     */
    login() {
        this.configService.config.channel.clientId = 'banistmoCNB';
        this.configService.config.channel.secret = 'YmFuaXN0bW9DTkI6c2VjcmV0QmFuaXN0bW9DTkI=';
        /** @type {?} */
        const bodyRequest = 'client_id=' + this.configService.config.channel.clientId + '&username=cristian.men&password=Colombia20.&grant_type=password';
        /** @type {?} */
        const type = 'POST';
        /** @type {?} */
        const url = this.API_LOGIN;
        /** @type {?} */
        const body = bodyRequest;
        /** @type {?} */
        const httpParams = null;
        return this.httpRequestService.httpRequest(type, url, body, httpParams);
    }
    /**
     * @param {?} username
     * @return {?}
     */
    veryfyUser(username) {
        /** @type {?} */
        const type = 'GET';
        /** @type {?} */
        const url = this.API_USER;
        /** @type {?} */
        const body = null;
        /** @type {?} */
        const httpParams = null;
        return this.httpRequestService.httpRequest(type, url, body, httpParams);
    }
    /**
     * @return {?}
     */
    getApiUser() {
        return this.API_USER;
    }
    /**
     * @param {?} API_USER
     * @return {?}
     */
    setApiUser(API_USER) {
        this.API_USER = API_USER;
    }
}
UserService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
UserService.ctorParameters = () => [
    { type: HttpRequestService },
    { type: LoginService },
    { type: ConfigService }
];
/** @nocollapse */ UserService.ngInjectableDef = i0.defineInjectable({ factory: function UserService_Factory() { return new UserService(i0.inject(i1.HttpRequestService), i0.inject(i2.LoginService), i0.inject(i3.ConfigService)); }, token: UserService, providedIn: "root" });
function UserService_tsickle_Closure_declarations() {
    /** @type {?} */
    UserService.prototype.API_LOGIN;
    /** @type {?} */
    UserService.prototype.API_USER;
    /** @type {?} */
    UserService.prototype.httpRequestService;
    /** @type {?} */
    UserService.prototype.loginService;
    /** @type {?} */
    UserService.prototype.configService;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbG9naWMtbGliLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL3VzZXIvdXNlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBRXpFLE9BQU8sRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLE1BQU0scUJBQXFCLENBQUM7Ozs7O0FBS2xFLE1BQU07Ozs7OztJQUtKLFlBQ1Msb0JBQ0EsY0FDQTtRQUZBLHVCQUFrQixHQUFsQixrQkFBa0I7UUFDbEIsaUJBQVksR0FBWixZQUFZO1FBQ1osa0JBQWEsR0FBYixhQUFhO0tBQ2pCOzs7O0lBRUwsS0FBSztRQUVILElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsYUFBYSxDQUFDO1FBQzNELElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsMENBQTBDLENBQUM7O1FBRXRGLE1BQU0sV0FBVyxHQUFRLFlBQVksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsUUFBUSxHQUFHLGlFQUFpRSxDQUFDOztRQUV2SixNQUFNLElBQUksR0FBRyxNQUFNLENBQUM7O1FBQ3BCLE1BQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7O1FBQzNCLE1BQU0sSUFBSSxHQUFHLFdBQVcsQ0FBQzs7UUFDekIsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLE1BQU0sQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0tBQ3pFOzs7OztJQUVELFVBQVUsQ0FBQyxRQUFhOztRQUN0QixNQUFNLElBQUksR0FBRyxLQUFLLENBQUM7O1FBQ25CLE1BQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7O1FBQzFCLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQzs7UUFDbEIsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLE1BQU0sQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0tBQ3pFOzs7O0lBRUQsVUFBVTtRQUNSLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO0tBQ3RCOzs7OztJQUVELFVBQVUsQ0FBQyxRQUFnQjtRQUN6QixJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztLQUMxQjs7O1lBMUNGLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7OztZQU5RLGtCQUFrQjtZQUVsQixZQUFZO1lBQUUsYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBSZXF1ZXN0U2VydmljZSB9IGZyb20gJy4uL2h0dHBSZXF1ZXN0L2h0dHAtcmVxdWVzdC5zZXJ2aWNlJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL2ludGVybmFsL09ic2VydmFibGUnO1xuaW1wb3J0IHsgTG9naW5TZXJ2aWNlLCBDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vcHVibGljX2FwaSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFVzZXJTZXJ2aWNlIHtcblxuICBwcml2YXRlIEFQSV9MT0dJTjogc3RyaW5nO1xuICBwcml2YXRlIEFQSV9VU0VSOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHVibGljIGh0dHBSZXF1ZXN0U2VydmljZTogSHR0cFJlcXVlc3RTZXJ2aWNlLFxuICAgIHB1YmxpYyBsb2dpblNlcnZpY2U6IExvZ2luU2VydmljZSxcbiAgICBwdWJsaWMgY29uZmlnU2VydmljZTogQ29uZmlnU2VydmljZVxuICApIHsgfVxuXG4gIGxvZ2luKCkge1xuXG4gICAgdGhpcy5jb25maWdTZXJ2aWNlLmNvbmZpZy5jaGFubmVsLmNsaWVudElkID0gJ2JhbmlzdG1vQ05CJztcbiAgICB0aGlzLmNvbmZpZ1NlcnZpY2UuY29uZmlnLmNoYW5uZWwuc2VjcmV0ID0gJ1ltRnVhWE4wYlc5RFRrSTZjMlZqY21WMFFtRnVhWE4wYlc5RFRrST0nO1xuICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTptYXgtbGluZS1sZW5ndGhcbiAgICBjb25zdCBib2R5UmVxdWVzdDogYW55ID0gJ2NsaWVudF9pZD0nICsgdGhpcy5jb25maWdTZXJ2aWNlLmNvbmZpZy5jaGFubmVsLmNsaWVudElkICsgJyZ1c2VybmFtZT1jcmlzdGlhbi5tZW4mcGFzc3dvcmQ9Q29sb21iaWEyMC4mZ3JhbnRfdHlwZT1wYXNzd29yZCc7XG5cbiAgICBjb25zdCB0eXBlID0gJ1BPU1QnO1xuICAgIGNvbnN0IHVybCA9IHRoaXMuQVBJX0xPR0lOO1xuICAgIGNvbnN0IGJvZHkgPSBib2R5UmVxdWVzdDtcbiAgICBjb25zdCBodHRwUGFyYW1zID0gbnVsbDtcbiAgICByZXR1cm4gdGhpcy5odHRwUmVxdWVzdFNlcnZpY2UuaHR0cFJlcXVlc3QodHlwZSwgdXJsLCBib2R5LCBodHRwUGFyYW1zKTtcbiAgfVxuXG4gIHZlcnlmeVVzZXIodXNlcm5hbWU6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgY29uc3QgdHlwZSA9ICdHRVQnO1xuICAgIGNvbnN0IHVybCA9IHRoaXMuQVBJX1VTRVI7XG4gICAgY29uc3QgYm9keSA9IG51bGw7XG4gICAgY29uc3QgaHR0cFBhcmFtcyA9IG51bGw7XG4gICAgcmV0dXJuIHRoaXMuaHR0cFJlcXVlc3RTZXJ2aWNlLmh0dHBSZXF1ZXN0KHR5cGUsIHVybCwgYm9keSwgaHR0cFBhcmFtcyk7XG4gIH1cblxuICBnZXRBcGlVc2VyKCkge1xuICAgIHJldHVybiB0aGlzLkFQSV9VU0VSO1xuICB9XG5cbiAgc2V0QXBpVXNlcihBUElfVVNFUjogc3RyaW5nKSB7XG4gICAgdGhpcy5BUElfVVNFUiA9IEFQSV9VU0VSO1xuICB9XG59XG4iXX0=