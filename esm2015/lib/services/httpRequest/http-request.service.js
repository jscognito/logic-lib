/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
export class HttpRequestService {
    /**
     * @param {?} httpClient
     */
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    /**
     * @param {?} typeRequest
     * @param {?} url
     * @param {?} bodyRequest
     * @param {?} queryString
     * @return {?}
     */
    httpRequest(typeRequest, url, bodyRequest, queryString) {
        return this.httpClient.request(typeRequest, url, { body: bodyRequest, observe: 'response', params: queryString });
    }
}
HttpRequestService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
HttpRequestService.ctorParameters = () => [
    { type: HttpClient }
];
/** @nocollapse */ HttpRequestService.ngInjectableDef = i0.defineInjectable({ factory: function HttpRequestService_Factory() { return new HttpRequestService(i0.inject(i1.HttpClient)); }, token: HttpRequestService, providedIn: "root" });
function HttpRequestService_tsickle_Closure_declarations() {
    /** @type {?} */
    HttpRequestService.prototype.httpClient;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHR0cC1yZXF1ZXN0LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9sb2dpYy1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvaHR0cFJlcXVlc3QvaHR0cC1yZXF1ZXN0LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBMEIsTUFBTSxzQkFBc0IsQ0FBQzs7O0FBTTFFLE1BQU07Ozs7SUFFSixZQUFtQixVQUFzQjtRQUF0QixlQUFVLEdBQVYsVUFBVSxDQUFZO0tBQUs7Ozs7Ozs7O0lBRXZDLFdBQVcsQ0FBQyxXQUFtQixFQUFFLEdBQVcsRUFBRSxXQUFnQixFQUFFLFdBQXVCO1FBRzVGLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsR0FBRyxFQUFFLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxXQUFXLEVBQUUsQ0FBQyxDQUFDOzs7O1lBVnJILFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7OztZQUxRLFVBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycywgSHR0cFBhcmFtc30gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvT2JzZXJ2YWJsZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIEh0dHBSZXF1ZXN0U2VydmljZSB7XG5cbiAgY29uc3RydWN0b3IocHVibGljIGh0dHBDbGllbnQ6IEh0dHBDbGllbnQpIHsgfVxuXG4gIHB1YmxpYyBodHRwUmVxdWVzdCh0eXBlUmVxdWVzdDogc3RyaW5nLCB1cmw6IHN0cmluZywgYm9keVJlcXVlc3Q6IGFueSwgcXVlcnlTdHJpbmc6IEh0dHBQYXJhbXMpOiBPYnNlcnZhYmxlIDxhbnk+ICB7XG5cbiAgICBcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnJlcXVlc3QodHlwZVJlcXVlc3QsIHVybCwgeyBib2R5OiBib2R5UmVxdWVzdCwgb2JzZXJ2ZTogJ3Jlc3BvbnNlJywgcGFyYW1zOiBxdWVyeVN0cmluZyB9KTtcbiAgfVxuXG59XG4iXX0=