/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import { ConfigService } from '../config/config.service';
import { SessionService } from '../session/session.service';
import * as i0 from "@angular/core";
import * as i1 from "../config/config.service";
import * as i2 from "../session/session.service";
var InterceptorService = /** @class */ (function () {
    function InterceptorService(configService, sessionService) {
        this.configService = configService;
        this.sessionService = sessionService;
    }
    /**
     * Método interceptor
     * @param request
     * @param next
     */
    /**
     * Método interceptor
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    InterceptorService.prototype.intercept = /**
     * Método interceptor
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    function (request, next) {
        var _this = this;
        /** @type {?} */
        var authorization;
        /** @type {?} */
        var contentType;
        if (this.configService.config.token.value === null) {
            authorization = 'Basic' + ' ' + this.configService.config.channel.secret;
            contentType = 'application/x-www-form-urlencoded';
        }
        else {
            authorization = this.configService.config.token.type + ' ' + this.configService.config.token.value;
            contentType = 'application/json';
        }
        request = request.clone({
            setHeaders: {
                'Authorization': '' + authorization,
                'Content-Type': '' + contentType,
                'Audit': this.sessionService.session.username === null ? '' : '' + this.sessionService.session.username
            }
        });
        return next.handle(request)
            .catch(function (error) {
            {
                return _this.handle400Error(error);
            }
        });
    };
    /**
     * @param {?} error
     * @return {?}
     */
    InterceptorService.prototype.handle400Error = /**
     * @param {?} error
     * @return {?}
     */
    function (error) {
        try {
            return Observable.throw(error);
        }
        catch (e) {
            return Observable.throw(error);
        }
    };
    InterceptorService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] },
    ];
    /** @nocollapse */
    InterceptorService.ctorParameters = function () { return [
        { type: ConfigService },
        { type: SessionService }
    ]; };
    /** @nocollapse */ InterceptorService.ngInjectableDef = i0.defineInjectable({ factory: function InterceptorService_Factory() { return new InterceptorService(i0.inject(i1.ConfigService), i0.inject(i2.SessionService)); }, token: InterceptorService, providedIn: "root" });
    return InterceptorService;
}());
export { InterceptorService };
function InterceptorService_tsickle_Closure_declarations() {
    /** @type {?} */
    InterceptorService.prototype.configService;
    /** @type {?} */
    InterceptorService.prototype.sessionService;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJjZXB0b3Iuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2xvZ2ljLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlcy9pbnRlcmNlcHRvci9pbnRlcmNlcHRvci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFDQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDbEMsT0FBTyxzQkFBc0IsQ0FBQztBQUM5QixPQUFPLHlCQUF5QixDQUFDO0FBQ2pDLE9BQU8saUJBQWlCLENBQUM7QUFDekIsT0FBTywyQkFBMkIsQ0FBQztBQUNuQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDekQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDRCQUE0QixDQUFDOzs7OztJQVExRCw0QkFDUyxlQUNBO1FBREEsa0JBQWEsR0FBYixhQUFhO1FBQ2IsbUJBQWMsR0FBZCxjQUFjO0tBQ3RCO0lBRUQ7Ozs7T0FJRzs7Ozs7OztJQUNILHNDQUFTOzs7Ozs7SUFBVCxVQUFVLE9BQXlCLEVBQUUsSUFBaUI7UUFBdEQsaUJBeUJDOztRQXhCQyxJQUFJLGFBQWEsQ0FBQzs7UUFDbEIsSUFBSSxXQUFXLENBQUM7UUFFaEIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ25ELGFBQWEsR0FBRyxPQUFPLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUM7WUFDekUsV0FBVyxHQUFHLG1DQUFtQyxDQUFDO1NBQ25EO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixhQUFhLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUNuRyxXQUFXLEdBQUcsa0JBQWtCLENBQUM7U0FDbEM7UUFFRCxPQUFPLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztZQUN0QixVQUFVLEVBQUU7Z0JBQ1YsZUFBZSxFQUFDLEVBQUUsR0FBRSxhQUFhO2dCQUNqQyxjQUFjLEVBQUMsRUFBRSxHQUFFLFdBQVc7Z0JBQzlCLE9BQU8sRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxRQUFRLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUEsQ0FBQyxDQUFBLEVBQUUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxRQUFRO2FBQ3RHO1NBQ0YsQ0FBQyxDQUFDO1FBQ0gsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDO2FBQ3hCLEtBQUssQ0FBQyxVQUFBLEtBQUs7WUFDVixDQUFDO2dCQUNDLE1BQU0sQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ25DO1NBQ0YsQ0FBQyxDQUFDO0tBQ047Ozs7O0lBRUQsMkNBQWM7Ozs7SUFBZCxVQUFlLEtBQUs7UUFDbEIsSUFBSSxDQUFDO1lBQ0gsTUFBTSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDaEM7UUFBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUNYLE1BQU0sQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2hDO0tBRUY7O2dCQWxERixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7O2dCQUxRLGFBQWE7Z0JBQ2IsY0FBYzs7OzZCQVZ2Qjs7U0FlYSxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBJbnRlcmNlcHRvciB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwL3NyYy9pbnRlcmNlcHRvcic7XG5pbXBvcnQgeyBIdHRwUmVxdWVzdCwgSHR0cEhhbmRsZXIsIEh0dHBFdmVudCwgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgJ3J4anMvYWRkL29wZXJhdG9yL2RvJztcbmltcG9ydCAncnhqcy9hZGQvb3BlcmF0b3IvY2F0Y2gnO1xuaW1wb3J0ICdyeGpzL09ic2VydmFibGUnO1xuaW1wb3J0ICdyeGpzL2FkZC9vYnNlcnZhYmxlL3Rocm93JztcbmltcG9ydCB7IENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi9jb25maWcvY29uZmlnLnNlcnZpY2UnO1xuaW1wb3J0IHsgU2Vzc2lvblNlcnZpY2UgfSBmcm9tICcuLi9zZXNzaW9uL3Nlc3Npb24uc2VydmljZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIEludGVyY2VwdG9yU2VydmljZSBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XG5cblxuICBjb25zdHJ1Y3RvcihcbiAgICBwdWJsaWMgY29uZmlnU2VydmljZTogQ29uZmlnU2VydmljZSxcbiAgICBwdWJsaWMgc2Vzc2lvblNlcnZpY2U6IFNlc3Npb25TZXJ2aWNlICkge1xuICB9XG5cbiAgLyoqXG4gICAqIE3DqXRvZG8gaW50ZXJjZXB0b3JcbiAgICogQHBhcmFtIHJlcXVlc3QgXG4gICAqIEBwYXJhbSBuZXh0IFxuICAgKi9cbiAgaW50ZXJjZXB0KHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xuICAgIGxldCBhdXRob3JpemF0aW9uO1xuICAgIGxldCBjb250ZW50VHlwZTtcbiAgICBcbiAgICBpZiAodGhpcy5jb25maWdTZXJ2aWNlLmNvbmZpZy50b2tlbi52YWx1ZSA9PT0gbnVsbCkge1xuICAgICAgYXV0aG9yaXphdGlvbiA9ICdCYXNpYycgKyAnICcgKyB0aGlzLmNvbmZpZ1NlcnZpY2UuY29uZmlnLmNoYW5uZWwuc2VjcmV0O1xuICAgICAgY29udGVudFR5cGUgPSAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJztcbiAgICB9IGVsc2Uge1xuICAgICAgYXV0aG9yaXphdGlvbiA9IHRoaXMuY29uZmlnU2VydmljZS5jb25maWcudG9rZW4udHlwZSArICcgJyArIHRoaXMuY29uZmlnU2VydmljZS5jb25maWcudG9rZW4udmFsdWU7XG4gICAgICBjb250ZW50VHlwZSA9ICdhcHBsaWNhdGlvbi9qc29uJztcbiAgICB9XG4gICAgXG4gICAgcmVxdWVzdCA9IHJlcXVlc3QuY2xvbmUoe1xuICAgICAgc2V0SGVhZGVyczoge1xuICAgICAgICAnQXV0aG9yaXphdGlvbic6JycrIGF1dGhvcml6YXRpb24sXG4gICAgICAgICdDb250ZW50LVR5cGUnOicnKyBjb250ZW50VHlwZSxcbiAgICAgICAgJ0F1ZGl0JzogdGhpcy5zZXNzaW9uU2VydmljZS5zZXNzaW9uLnVzZXJuYW1lID09PSBudWxsID8gJyc6JycgKyB0aGlzLnNlc3Npb25TZXJ2aWNlLnNlc3Npb24udXNlcm5hbWVcbiAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdClcbiAgICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHtcbiAgICAgICAgICByZXR1cm4gdGhpcy5oYW5kbGU0MDBFcnJvcihlcnJvcik7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICB9XG4gXG4gIGhhbmRsZTQwMEVycm9yKGVycm9yKSB7XG4gICAgdHJ5IHtcbiAgICAgIHJldHVybiBPYnNlcnZhYmxlLnRocm93KGVycm9yKTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICByZXR1cm4gT2JzZXJ2YWJsZS50aHJvdyhlcnJvcik7XG4gICAgfVxuXG4gIH1cblxufVxuIl19