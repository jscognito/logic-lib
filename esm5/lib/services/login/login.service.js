/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpRequestService } from '../httpRequest/http-request.service';
import * as i0 from "@angular/core";
import * as i1 from "../httpRequest/http-request.service";
var LoginService = /** @class */ (function () {
    function LoginService(httpRequestService) {
        this.httpRequestService = httpRequestService;
    }
    /**
     * @param {?} bodyRequest
     * @return {?}
     */
    LoginService.prototype.login = /**
     * @param {?} bodyRequest
     * @return {?}
     */
    function (bodyRequest) {
        /** @type {?} */
        var type = 'POST';
        /** @type {?} */
        var url = this.API_LOGIN;
        /** @type {?} */
        var body = bodyRequest;
        /** @type {?} */
        var httpParams = null;
        return this.httpRequestService.httpRequest(type, url, body, httpParams);
    };
    /**
     * @return {?}
     */
    LoginService.prototype.getApiLogin = /**
     * @return {?}
     */
    function () {
        return this.API_LOGIN;
    };
    /**
     * @param {?} API_LOGIN
     * @return {?}
     */
    LoginService.prototype.setApiLogin = /**
     * @param {?} API_LOGIN
     * @return {?}
     */
    function (API_LOGIN) {
        this.API_LOGIN = API_LOGIN;
    };
    LoginService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] },
    ];
    /** @nocollapse */
    LoginService.ctorParameters = function () { return [
        { type: HttpRequestService }
    ]; };
    /** @nocollapse */ LoginService.ngInjectableDef = i0.defineInjectable({ factory: function LoginService_Factory() { return new LoginService(i0.inject(i1.HttpRequestService)); }, token: LoginService, providedIn: "root" });
    return LoginService;
}());
export { LoginService };
function LoginService_tsickle_Closure_declarations() {
    /** @type {?} */
    LoginService.prototype.API_LOGIN;
    /** @type {?} */
    LoginService.prototype.httpRequestService;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2xvZ2ljLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlcy9sb2dpbi9sb2dpbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHFDQUFxQyxDQUFDOzs7O0lBV3ZFLHNCQUFtQixrQkFBc0M7UUFBdEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtLQUFLOzs7OztJQUU5RCw0QkFBSzs7OztJQUFMLFVBQU0sV0FBZ0I7O1FBQ3BCLElBQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQzs7UUFDcEIsSUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQzs7UUFDM0IsSUFBTSxJQUFJLEdBQUcsV0FBVyxDQUFDOztRQUN6QixJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFDeEIsTUFBTSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUM7S0FDekU7Ozs7SUFFRCxrQ0FBVzs7O0lBQVg7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztLQUN2Qjs7Ozs7SUFFRCxrQ0FBVzs7OztJQUFYLFVBQVksU0FBaUI7UUFDM0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7S0FDNUI7O2dCQXZCRixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7O2dCQU5RLGtCQUFrQjs7O3VCQUQzQjs7U0FRYSxZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cFJlcXVlc3RTZXJ2aWNlIH0gZnJvbSAnLi4vaHR0cFJlcXVlc3QvaHR0cC1yZXF1ZXN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvaW50ZXJuYWwvT2JzZXJ2YWJsZSc7XG5cblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgTG9naW5TZXJ2aWNlIHtcblxuICBwcml2YXRlIEFQSV9MT0dJTjogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBodHRwUmVxdWVzdFNlcnZpY2U6IEh0dHBSZXF1ZXN0U2VydmljZSkgeyB9XG5cbiAgbG9naW4oYm9keVJlcXVlc3Q6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgY29uc3QgdHlwZSA9ICdQT1NUJztcbiAgICBjb25zdCB1cmwgPSB0aGlzLkFQSV9MT0dJTjtcbiAgICBjb25zdCBib2R5ID0gYm9keVJlcXVlc3Q7XG4gICAgY29uc3QgaHR0cFBhcmFtcyA9IG51bGw7XG4gICAgcmV0dXJuIHRoaXMuaHR0cFJlcXVlc3RTZXJ2aWNlLmh0dHBSZXF1ZXN0KHR5cGUsIHVybCwgYm9keSwgaHR0cFBhcmFtcyk7XG4gIH1cblxuICBnZXRBcGlMb2dpbigpIHtcbiAgICByZXR1cm4gdGhpcy5BUElfTE9HSU47XG4gIH1cblxuICBzZXRBcGlMb2dpbihBUElfTE9HSU46IHN0cmluZykge1xuICAgIHRoaXMuQVBJX0xPR0lOID0gQVBJX0xPR0lOO1xuICB9XG59XG4iXX0=