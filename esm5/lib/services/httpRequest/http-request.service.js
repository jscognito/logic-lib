/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
var HttpRequestService = /** @class */ (function () {
    function HttpRequestService(httpClient) {
        this.httpClient = httpClient;
    }
    /**
     * @param {?} typeRequest
     * @param {?} url
     * @param {?} bodyRequest
     * @param {?} queryString
     * @return {?}
     */
    HttpRequestService.prototype.httpRequest = /**
     * @param {?} typeRequest
     * @param {?} url
     * @param {?} bodyRequest
     * @param {?} queryString
     * @return {?}
     */
    function (typeRequest, url, bodyRequest, queryString) {
        return this.httpClient.request(typeRequest, url, { body: bodyRequest, observe: 'response', params: queryString });
    };
    HttpRequestService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] },
    ];
    /** @nocollapse */
    HttpRequestService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    /** @nocollapse */ HttpRequestService.ngInjectableDef = i0.defineInjectable({ factory: function HttpRequestService_Factory() { return new HttpRequestService(i0.inject(i1.HttpClient)); }, token: HttpRequestService, providedIn: "root" });
    return HttpRequestService;
}());
export { HttpRequestService };
function HttpRequestService_tsickle_Closure_declarations() {
    /** @type {?} */
    HttpRequestService.prototype.httpClient;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHR0cC1yZXF1ZXN0LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9sb2dpYy1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvaHR0cFJlcXVlc3QvaHR0cC1yZXF1ZXN0LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBMEIsTUFBTSxzQkFBc0IsQ0FBQzs7OztJQVF4RSw0QkFBbUIsVUFBc0I7UUFBdEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtLQUFLOzs7Ozs7OztJQUV2Qyx3Q0FBVzs7Ozs7OztjQUFDLFdBQW1CLEVBQUUsR0FBVyxFQUFFLFdBQWdCLEVBQUUsV0FBdUI7UUFHNUYsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxHQUFHLEVBQUUsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLFdBQVcsRUFBRSxDQUFDLENBQUM7OztnQkFWckgsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7OztnQkFMUSxVQUFVOzs7NkJBRG5COztTQU9hLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzLCBIdHRwUGFyYW1zfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9PYnNlcnZhYmxlJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgSHR0cFJlcXVlc3RTZXJ2aWNlIHtcblxuICBjb25zdHJ1Y3RvcihwdWJsaWMgaHR0cENsaWVudDogSHR0cENsaWVudCkgeyB9XG5cbiAgcHVibGljIGh0dHBSZXF1ZXN0KHR5cGVSZXF1ZXN0OiBzdHJpbmcsIHVybDogc3RyaW5nLCBib2R5UmVxdWVzdDogYW55LCBxdWVyeVN0cmluZzogSHR0cFBhcmFtcyk6IE9ic2VydmFibGUgPGFueT4gIHtcblxuICAgIFxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucmVxdWVzdCh0eXBlUmVxdWVzdCwgdXJsLCB7IGJvZHk6IGJvZHlSZXF1ZXN0LCBvYnNlcnZlOiAncmVzcG9uc2UnLCBwYXJhbXM6IHF1ZXJ5U3RyaW5nIH0pO1xuICB9XG5cbn1cbiJdfQ==