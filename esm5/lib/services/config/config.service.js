/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var ConfigService = /** @class */ (function () {
    function ConfigService() {
        this.config = {
            token: {
                type: null,
                value: null
            },
            channel: {
                clientId: null,
                grantType: null,
                secret: null,
            }
        };
    }
    ConfigService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] },
    ];
    /** @nocollapse */
    ConfigService.ctorParameters = function () { return []; };
    /** @nocollapse */ ConfigService.ngInjectableDef = i0.defineInjectable({ factory: function ConfigService_Factory() { return new ConfigService(); }, token: ConfigService, providedIn: "root" });
    return ConfigService;
}());
export { ConfigService };
function ConfigService_tsickle_Closure_declarations() {
    /** @type {?} */
    ConfigService.prototype.config;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9sb2dpYy1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvY29uZmlnL2NvbmZpZy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7SUFtQnpDO3NCQVpnQjtZQUNkLEtBQUssRUFBRTtnQkFDTCxJQUFJLEVBQUUsSUFBSTtnQkFDVixLQUFLLEVBQUUsSUFBSTthQUNaO1lBQ0QsT0FBTyxFQUFFO2dCQUNQLFFBQVEsRUFBRSxJQUFJO2dCQUNkLFNBQVMsRUFBRSxJQUFJO2dCQUNmLE1BQU0sRUFBRSxJQUFJO2FBQ2I7U0FDRjtLQUVnQjs7Z0JBakJsQixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7Ozt3QkFKRDs7U0FLYSxhQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBDb25maWdTZXJ2aWNlIHtcblxuICBwdWJsaWMgY29uZmlnID0ge1xuICAgIHRva2VuOiB7XG4gICAgICB0eXBlOiBudWxsLFxuICAgICAgdmFsdWU6IG51bGxcbiAgICB9LFxuICAgIGNoYW5uZWw6IHtcbiAgICAgIGNsaWVudElkOiBudWxsLFxuICAgICAgZ3JhbnRUeXBlOiBudWxsLFxuICAgICAgc2VjcmV0OiBudWxsLFxuICAgIH1cbiAgfTtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxufVxuIl19