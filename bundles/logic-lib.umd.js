(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('rxjs'), require('rxjs/add/operator/do'), require('rxjs/add/operator/catch'), require('rxjs/Observable'), require('rxjs/add/observable/throw'), require('@angular/common/http')) :
    typeof define === 'function' && define.amd ? define('logic-lib', ['exports', '@angular/core', 'rxjs', 'rxjs/add/operator/do', 'rxjs/add/operator/catch', 'rxjs/Observable', 'rxjs/add/observable/throw', '@angular/common/http'], factory) :
    (factory((global['logic-lib'] = {}),global.ng.core,global.rxjs,global.rxjs['add/operator/do'],global.rxjs['add/operator/catch'],global.rxjs.Observable,global.rxjs['add/observable/throw'],global.ng.common.http));
}(this, (function (exports,i0,rxjs,_do,_catch,Observable,_throw,i1) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var LogicLibService = (function () {
        function LogicLibService() {
        }
        LogicLibService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] },
        ];
        /** @nocollapse */
        LogicLibService.ctorParameters = function () { return []; };
        /** @nocollapse */ LogicLibService.ngInjectableDef = i0.defineInjectable({ factory: function LogicLibService_Factory() { return new LogicLibService(); }, token: LogicLibService, providedIn: "root" });
        return LogicLibService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var LogicLibModule = (function () {
        function LogicLibModule() {
        }
        LogicLibModule.decorators = [
            { type: i0.NgModule, args: [{
                        imports: []
                    },] },
        ];
        return LogicLibModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var ConfigService = (function () {
        function ConfigService() {
            this.config = {
                token: {
                    type: null,
                    value: null
                },
                channel: {
                    clientId: null,
                    grantType: null,
                    secret: null,
                }
            };
        }
        ConfigService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] },
        ];
        /** @nocollapse */
        ConfigService.ctorParameters = function () { return []; };
        /** @nocollapse */ ConfigService.ngInjectableDef = i0.defineInjectable({ factory: function ConfigService_Factory() { return new ConfigService(); }, token: ConfigService, providedIn: "root" });
        return ConfigService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var SessionService = (function () {
        function SessionService() {
            this.session = {
                username: null
            };
        }
        SessionService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] },
        ];
        /** @nocollapse */
        SessionService.ctorParameters = function () { return []; };
        /** @nocollapse */ SessionService.ngInjectableDef = i0.defineInjectable({ factory: function SessionService_Factory() { return new SessionService(); }, token: SessionService, providedIn: "root" });
        return SessionService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var InterceptorService = (function () {
        function InterceptorService(configService, sessionService) {
            this.configService = configService;
            this.sessionService = sessionService;
        }
        /**
         * Método interceptor
         * @param request
         * @param next
         */
        /**
         * Método interceptor
         * @param {?} request
         * @param {?} next
         * @return {?}
         */
        InterceptorService.prototype.intercept = /**
         * Método interceptor
         * @param {?} request
         * @param {?} next
         * @return {?}
         */
            function (request, next) {
                var _this = this;
                /** @type {?} */
                var authorization;
                /** @type {?} */
                var contentType;
                if (this.configService.config.token.value === null) {
                    authorization = 'Basic' + ' ' + this.configService.config.channel.secret;
                    contentType = 'application/x-www-form-urlencoded';
                }
                else {
                    authorization = this.configService.config.token.type + ' ' + this.configService.config.token.value;
                    contentType = 'application/json';
                }
                request = request.clone({
                    setHeaders: {
                        'Authorization': '' + authorization,
                        'Content-Type': '' + contentType,
                        'Audit': this.sessionService.session.username === null ? '' : '' + this.sessionService.session.username
                    }
                });
                return next.handle(request)
                    .catch(function (error) {
                    {
                        return _this.handle400Error(error);
                    }
                });
            };
        /**
         * @param {?} error
         * @return {?}
         */
        InterceptorService.prototype.handle400Error = /**
         * @param {?} error
         * @return {?}
         */
            function (error) {
                try {
                    return rxjs.Observable.throw(error);
                }
                catch (e) {
                    return rxjs.Observable.throw(error);
                }
            };
        InterceptorService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] },
        ];
        /** @nocollapse */
        InterceptorService.ctorParameters = function () {
            return [
                { type: ConfigService },
                { type: SessionService }
            ];
        };
        /** @nocollapse */ InterceptorService.ngInjectableDef = i0.defineInjectable({ factory: function InterceptorService_Factory() { return new InterceptorService(i0.inject(ConfigService), i0.inject(SessionService)); }, token: InterceptorService, providedIn: "root" });
        return InterceptorService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var HttpRequestService = (function () {
        function HttpRequestService(httpClient) {
            this.httpClient = httpClient;
        }
        /**
         * @param {?} typeRequest
         * @param {?} url
         * @param {?} bodyRequest
         * @param {?} queryString
         * @return {?}
         */
        HttpRequestService.prototype.httpRequest = /**
         * @param {?} typeRequest
         * @param {?} url
         * @param {?} bodyRequest
         * @param {?} queryString
         * @return {?}
         */
            function (typeRequest, url, bodyRequest, queryString) {
                return this.httpClient.request(typeRequest, url, { body: bodyRequest, observe: 'response', params: queryString });
            };
        HttpRequestService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] },
        ];
        /** @nocollapse */
        HttpRequestService.ctorParameters = function () {
            return [
                { type: i1.HttpClient }
            ];
        };
        /** @nocollapse */ HttpRequestService.ngInjectableDef = i0.defineInjectable({ factory: function HttpRequestService_Factory() { return new HttpRequestService(i0.inject(i1.HttpClient)); }, token: HttpRequestService, providedIn: "root" });
        return HttpRequestService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var LoginService = (function () {
        function LoginService(httpRequestService) {
            this.httpRequestService = httpRequestService;
        }
        /**
         * @param {?} bodyRequest
         * @return {?}
         */
        LoginService.prototype.login = /**
         * @param {?} bodyRequest
         * @return {?}
         */
            function (bodyRequest) {
                /** @type {?} */
                var type = 'POST';
                /** @type {?} */
                var url = this.API_LOGIN;
                /** @type {?} */
                var body = bodyRequest;
                /** @type {?} */
                var httpParams = null;
                return this.httpRequestService.httpRequest(type, url, body, httpParams);
            };
        /**
         * @return {?}
         */
        LoginService.prototype.getApiLogin = /**
         * @return {?}
         */
            function () {
                return this.API_LOGIN;
            };
        /**
         * @param {?} API_LOGIN
         * @return {?}
         */
        LoginService.prototype.setApiLogin = /**
         * @param {?} API_LOGIN
         * @return {?}
         */
            function (API_LOGIN) {
                this.API_LOGIN = API_LOGIN;
            };
        LoginService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] },
        ];
        /** @nocollapse */
        LoginService.ctorParameters = function () {
            return [
                { type: HttpRequestService }
            ];
        };
        /** @nocollapse */ LoginService.ngInjectableDef = i0.defineInjectable({ factory: function LoginService_Factory() { return new LoginService(i0.inject(HttpRequestService)); }, token: LoginService, providedIn: "root" });
        return LoginService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var LogoutService = (function () {
        function LogoutService(httpRequestService) {
            this.httpRequestService = httpRequestService;
        }
        /**
         * @return {?}
         */
        LogoutService.prototype.logout = /**
         * @return {?}
         */
            function () {
                /** @type {?} */
                var type = 'POST';
                /** @type {?} */
                var url = this.API_LOGOUT;
                /** @type {?} */
                var body = null;
                /** @type {?} */
                var httpParams = null;
                return this.httpRequestService.httpRequest(type, url, body, httpParams);
            };
        /**
         * @return {?}
         */
        LogoutService.prototype.getApiLogout = /**
         * @return {?}
         */
            function () {
                return this.API_LOGOUT;
            };
        /**
         * @param {?} API_LOGIN
         * @return {?}
         */
        LogoutService.prototype.setApiLogout = /**
         * @param {?} API_LOGIN
         * @return {?}
         */
            function (API_LOGIN) {
                this.API_LOGOUT = API_LOGIN;
            };
        LogoutService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] },
        ];
        /** @nocollapse */
        LogoutService.ctorParameters = function () {
            return [
                { type: HttpRequestService }
            ];
        };
        /** @nocollapse */ LogoutService.ngInjectableDef = i0.defineInjectable({ factory: function LogoutService_Factory() { return new LogoutService(i0.inject(HttpRequestService)); }, token: LogoutService, providedIn: "root" });
        return LogoutService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var UserService$$1 = (function () {
        function UserService$$1(httpRequestService, loginService, configService) {
            this.httpRequestService = httpRequestService;
            this.loginService = loginService;
            this.configService = configService;
        }
        /**
         * @return {?}
         */
        UserService$$1.prototype.login = /**
         * @return {?}
         */
            function () {
                this.configService.config.channel.clientId = 'banistmoCNB';
                this.configService.config.channel.secret = 'YmFuaXN0bW9DTkI6c2VjcmV0QmFuaXN0bW9DTkI=';
                /** @type {?} */
                var bodyRequest = 'client_id=' + this.configService.config.channel.clientId + '&username=cristian.men&password=Colombia20.&grant_type=password';
                /** @type {?} */
                var type = 'POST';
                /** @type {?} */
                var url = this.API_LOGIN;
                /** @type {?} */
                var body = bodyRequest;
                /** @type {?} */
                var httpParams = null;
                return this.httpRequestService.httpRequest(type, url, body, httpParams);
            };
        /**
         * @param {?} username
         * @return {?}
         */
        UserService$$1.prototype.veryfyUser = /**
         * @param {?} username
         * @return {?}
         */
            function (username) {
                /** @type {?} */
                var type = 'GET';
                /** @type {?} */
                var url = this.API_USER;
                /** @type {?} */
                var body = null;
                /** @type {?} */
                var httpParams = null;
                return this.httpRequestService.httpRequest(type, url, body, httpParams);
            };
        /**
         * @return {?}
         */
        UserService$$1.prototype.getApiUser = /**
         * @return {?}
         */
            function () {
                return this.API_USER;
            };
        /**
         * @param {?} API_USER
         * @return {?}
         */
        UserService$$1.prototype.setApiUser = /**
         * @param {?} API_USER
         * @return {?}
         */
            function (API_USER) {
                this.API_USER = API_USER;
            };
        UserService$$1.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] },
        ];
        /** @nocollapse */
        UserService$$1.ctorParameters = function () {
            return [
                { type: HttpRequestService },
                { type: LoginService },
                { type: ConfigService }
            ];
        };
        /** @nocollapse */ UserService$$1.ngInjectableDef = i0.defineInjectable({ factory: function UserService_Factory() { return new UserService$$1(i0.inject(HttpRequestService), i0.inject(LoginService), i0.inject(ConfigService)); }, token: UserService$$1, providedIn: "root" });
        return UserService$$1;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    exports.InterceptorService = InterceptorService;
    exports.LoginService = LoginService;
    exports.LogoutService = LogoutService;
    exports.ConfigService = ConfigService;
    exports.SessionService = SessionService;
    exports.UserService = UserService$$1;
    exports.LogicLibService = LogicLibService;
    exports.LogicLibModule = LogicLibModule;
    exports.ɵa = HttpRequestService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naWMtbGliLnVtZC5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vbG9naWMtbGliL2xpYi9sb2dpYy1saWIuc2VydmljZS50cyIsIm5nOi8vbG9naWMtbGliL2xpYi9sb2dpYy1saWIubW9kdWxlLnRzIiwibmc6Ly9sb2dpYy1saWIvbGliL3NlcnZpY2VzL2NvbmZpZy9jb25maWcuc2VydmljZS50cyIsIm5nOi8vbG9naWMtbGliL2xpYi9zZXJ2aWNlcy9zZXNzaW9uL3Nlc3Npb24uc2VydmljZS50cyIsIm5nOi8vbG9naWMtbGliL2xpYi9zZXJ2aWNlcy9pbnRlcmNlcHRvci9pbnRlcmNlcHRvci5zZXJ2aWNlLnRzIiwibmc6Ly9sb2dpYy1saWIvbGliL3NlcnZpY2VzL2h0dHBSZXF1ZXN0L2h0dHAtcmVxdWVzdC5zZXJ2aWNlLnRzIiwibmc6Ly9sb2dpYy1saWIvbGliL3NlcnZpY2VzL2xvZ2luL2xvZ2luLnNlcnZpY2UudHMiLCJuZzovL2xvZ2ljLWxpYi9saWIvc2VydmljZXMvbG9nb3V0L2xvZ291dC5zZXJ2aWNlLnRzIiwibmc6Ly9sb2dpYy1saWIvbGliL3NlcnZpY2VzL3VzZXIvdXNlci5zZXJ2aWNlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgTG9naWNMaWJTZXJ2aWNlIHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxufVxuIiwiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICBdXG59KVxuZXhwb3J0IGNsYXNzIExvZ2ljTGliTW9kdWxlIHsgfVxuIiwiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBDb25maWdTZXJ2aWNlIHtcblxuICBwdWJsaWMgY29uZmlnID0ge1xuICAgIHRva2VuOiB7XG4gICAgICB0eXBlOiBudWxsLFxuICAgICAgdmFsdWU6IG51bGxcbiAgICB9LFxuICAgIGNoYW5uZWw6IHtcbiAgICAgIGNsaWVudElkOiBudWxsLFxuICAgICAgZ3JhbnRUeXBlOiBudWxsLFxuICAgICAgc2VjcmV0OiBudWxsLFxuICAgIH1cbiAgfTtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxufVxuIiwiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBTZXNzaW9uU2VydmljZSB7XG5cbiAgcHVibGljIHNlc3Npb24gPSB7XG4gICAgdXNlcm5hbWU6IG51bGxcbiAgfTtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxufVxuIiwiXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwSW50ZXJjZXB0b3IgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cC9zcmMvaW50ZXJjZXB0b3InO1xuaW1wb3J0IHsgSHR0cFJlcXVlc3QsIEh0dHBIYW5kbGVyLCBIdHRwRXZlbnQsIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0ICdyeGpzL2FkZC9vcGVyYXRvci9kbyc7XG5pbXBvcnQgJ3J4anMvYWRkL29wZXJhdG9yL2NhdGNoJztcbmltcG9ydCAncnhqcy9PYnNlcnZhYmxlJztcbmltcG9ydCAncnhqcy9hZGQvb2JzZXJ2YWJsZS90aHJvdyc7XG5pbXBvcnQgeyBDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vY29uZmlnL2NvbmZpZy5zZXJ2aWNlJztcbmltcG9ydCB7IFNlc3Npb25TZXJ2aWNlIH0gZnJvbSAnLi4vc2Vzc2lvbi9zZXNzaW9uLnNlcnZpY2UnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBJbnRlcmNlcHRvclNlcnZpY2UgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xuXG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHVibGljIGNvbmZpZ1NlcnZpY2U6IENvbmZpZ1NlcnZpY2UsXG4gICAgcHVibGljIHNlc3Npb25TZXJ2aWNlOiBTZXNzaW9uU2VydmljZSApIHtcbiAgfVxuXG4gIC8qKlxuICAgKiBNw4PCqXRvZG8gaW50ZXJjZXB0b3JcbiAgICogQHBhcmFtIHJlcXVlc3QgXG4gICAqIEBwYXJhbSBuZXh0IFxuICAgKi9cbiAgaW50ZXJjZXB0KHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xuICAgIGxldCBhdXRob3JpemF0aW9uO1xuICAgIGxldCBjb250ZW50VHlwZTtcbiAgICBcbiAgICBpZiAodGhpcy5jb25maWdTZXJ2aWNlLmNvbmZpZy50b2tlbi52YWx1ZSA9PT0gbnVsbCkge1xuICAgICAgYXV0aG9yaXphdGlvbiA9ICdCYXNpYycgKyAnICcgKyB0aGlzLmNvbmZpZ1NlcnZpY2UuY29uZmlnLmNoYW5uZWwuc2VjcmV0O1xuICAgICAgY29udGVudFR5cGUgPSAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJztcbiAgICB9IGVsc2Uge1xuICAgICAgYXV0aG9yaXphdGlvbiA9IHRoaXMuY29uZmlnU2VydmljZS5jb25maWcudG9rZW4udHlwZSArICcgJyArIHRoaXMuY29uZmlnU2VydmljZS5jb25maWcudG9rZW4udmFsdWU7XG4gICAgICBjb250ZW50VHlwZSA9ICdhcHBsaWNhdGlvbi9qc29uJztcbiAgICB9XG4gICAgXG4gICAgcmVxdWVzdCA9IHJlcXVlc3QuY2xvbmUoe1xuICAgICAgc2V0SGVhZGVyczoge1xuICAgICAgICAnQXV0aG9yaXphdGlvbic6JycrIGF1dGhvcml6YXRpb24sXG4gICAgICAgICdDb250ZW50LVR5cGUnOicnKyBjb250ZW50VHlwZSxcbiAgICAgICAgJ0F1ZGl0JzogdGhpcy5zZXNzaW9uU2VydmljZS5zZXNzaW9uLnVzZXJuYW1lID09PSBudWxsID8gJyc6JycgKyB0aGlzLnNlc3Npb25TZXJ2aWNlLnNlc3Npb24udXNlcm5hbWVcbiAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdClcbiAgICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHtcbiAgICAgICAgICByZXR1cm4gdGhpcy5oYW5kbGU0MDBFcnJvcihlcnJvcik7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICB9XG4gXG4gIGhhbmRsZTQwMEVycm9yKGVycm9yKSB7XG4gICAgdHJ5IHtcbiAgICAgIHJldHVybiBPYnNlcnZhYmxlLnRocm93KGVycm9yKTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICByZXR1cm4gT2JzZXJ2YWJsZS50aHJvdyhlcnJvcik7XG4gICAgfVxuXG4gIH1cblxufVxuIiwiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMsIEh0dHBQYXJhbXN9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL09ic2VydmFibGUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBIdHRwUmVxdWVzdFNlcnZpY2Uge1xuXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBodHRwQ2xpZW50OiBIdHRwQ2xpZW50KSB7IH1cblxuICBwdWJsaWMgaHR0cFJlcXVlc3QodHlwZVJlcXVlc3Q6IHN0cmluZywgdXJsOiBzdHJpbmcsIGJvZHlSZXF1ZXN0OiBhbnksIHF1ZXJ5U3RyaW5nOiBIdHRwUGFyYW1zKTogT2JzZXJ2YWJsZSA8YW55PiAge1xuXG4gICAgXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5yZXF1ZXN0KHR5cGVSZXF1ZXN0LCB1cmwsIHsgYm9keTogYm9keVJlcXVlc3QsIG9ic2VydmU6ICdyZXNwb25zZScsIHBhcmFtczogcXVlcnlTdHJpbmcgfSk7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cFJlcXVlc3RTZXJ2aWNlIH0gZnJvbSAnLi4vaHR0cFJlcXVlc3QvaHR0cC1yZXF1ZXN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvaW50ZXJuYWwvT2JzZXJ2YWJsZSc7XG5cblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgTG9naW5TZXJ2aWNlIHtcblxuICBwcml2YXRlIEFQSV9MT0dJTjogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBodHRwUmVxdWVzdFNlcnZpY2U6IEh0dHBSZXF1ZXN0U2VydmljZSkgeyB9XG5cbiAgbG9naW4oYm9keVJlcXVlc3Q6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgY29uc3QgdHlwZSA9ICdQT1NUJztcbiAgICBjb25zdCB1cmwgPSB0aGlzLkFQSV9MT0dJTjtcbiAgICBjb25zdCBib2R5ID0gYm9keVJlcXVlc3Q7XG4gICAgY29uc3QgaHR0cFBhcmFtcyA9IG51bGw7XG4gICAgcmV0dXJuIHRoaXMuaHR0cFJlcXVlc3RTZXJ2aWNlLmh0dHBSZXF1ZXN0KHR5cGUsIHVybCwgYm9keSwgaHR0cFBhcmFtcyk7XG4gIH1cblxuICBnZXRBcGlMb2dpbigpIHtcbiAgICByZXR1cm4gdGhpcy5BUElfTE9HSU47XG4gIH1cblxuICBzZXRBcGlMb2dpbihBUElfTE9HSU46IHN0cmluZykge1xuICAgIHRoaXMuQVBJX0xPR0lOID0gQVBJX0xPR0lOO1xuICB9XG59XG4iLCJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwUmVxdWVzdFNlcnZpY2UgfSBmcm9tICcuLi9odHRwUmVxdWVzdC9odHRwLXJlcXVlc3Quc2VydmljZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9pbnRlcm5hbC9PYnNlcnZhYmxlJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgTG9nb3V0U2VydmljZSB7XG5cbiAgcHJpdmF0ZSBBUElfTE9HT1VUOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IocHVibGljIGh0dHBSZXF1ZXN0U2VydmljZTogSHR0cFJlcXVlc3RTZXJ2aWNlKSB7IH1cblxuICBsb2dvdXQoKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICBjb25zdCB0eXBlID0gJ1BPU1QnO1xuICAgIGNvbnN0IHVybCA9IHRoaXMuQVBJX0xPR09VVDtcbiAgICBjb25zdCBib2R5ID0gbnVsbDtcbiAgICBjb25zdCBodHRwUGFyYW1zID0gbnVsbDtcbiAgICByZXR1cm4gdGhpcy5odHRwUmVxdWVzdFNlcnZpY2UuaHR0cFJlcXVlc3QodHlwZSwgdXJsLCBib2R5LCBodHRwUGFyYW1zKTtcbiAgfVxuXG4gIGdldEFwaUxvZ291dCgpIHtcbiAgICByZXR1cm4gdGhpcy5BUElfTE9HT1VUO1xuICB9XG5cbiAgc2V0QXBpTG9nb3V0KEFQSV9MT0dJTjogc3RyaW5nKSB7XG4gICAgdGhpcy5BUElfTE9HT1VUID0gQVBJX0xPR0lOO1xuICB9XG59XG4iLCJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwUmVxdWVzdFNlcnZpY2UgfSBmcm9tICcuLi9odHRwUmVxdWVzdC9odHRwLXJlcXVlc3Quc2VydmljZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9pbnRlcm5hbC9PYnNlcnZhYmxlJztcbmltcG9ydCB7IExvZ2luU2VydmljZSwgQ29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3B1YmxpY19hcGknO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBVc2VyU2VydmljZSB7XG5cbiAgcHJpdmF0ZSBBUElfTE9HSU46IHN0cmluZztcbiAgcHJpdmF0ZSBBUElfVVNFUjogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHB1YmxpYyBodHRwUmVxdWVzdFNlcnZpY2U6IEh0dHBSZXF1ZXN0U2VydmljZSxcbiAgICBwdWJsaWMgbG9naW5TZXJ2aWNlOiBMb2dpblNlcnZpY2UsXG4gICAgcHVibGljIGNvbmZpZ1NlcnZpY2U6IENvbmZpZ1NlcnZpY2VcbiAgKSB7IH1cblxuICBsb2dpbigpIHtcblxuICAgIHRoaXMuY29uZmlnU2VydmljZS5jb25maWcuY2hhbm5lbC5jbGllbnRJZCA9ICdiYW5pc3Rtb0NOQic7XG4gICAgdGhpcy5jb25maWdTZXJ2aWNlLmNvbmZpZy5jaGFubmVsLnNlY3JldCA9ICdZbUZ1YVhOMGJXOURUa0k2YzJWamNtVjBRbUZ1YVhOMGJXOURUa0k9JztcbiAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bWF4LWxpbmUtbGVuZ3RoXG4gICAgY29uc3QgYm9keVJlcXVlc3Q6IGFueSA9ICdjbGllbnRfaWQ9JyArIHRoaXMuY29uZmlnU2VydmljZS5jb25maWcuY2hhbm5lbC5jbGllbnRJZCArICcmdXNlcm5hbWU9Y3Jpc3RpYW4ubWVuJnBhc3N3b3JkPUNvbG9tYmlhMjAuJmdyYW50X3R5cGU9cGFzc3dvcmQnO1xuXG4gICAgY29uc3QgdHlwZSA9ICdQT1NUJztcbiAgICBjb25zdCB1cmwgPSB0aGlzLkFQSV9MT0dJTjtcbiAgICBjb25zdCBib2R5ID0gYm9keVJlcXVlc3Q7XG4gICAgY29uc3QgaHR0cFBhcmFtcyA9IG51bGw7XG4gICAgcmV0dXJuIHRoaXMuaHR0cFJlcXVlc3RTZXJ2aWNlLmh0dHBSZXF1ZXN0KHR5cGUsIHVybCwgYm9keSwgaHR0cFBhcmFtcyk7XG4gIH1cblxuICB2ZXJ5ZnlVc2VyKHVzZXJuYW1lOiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIGNvbnN0IHR5cGUgPSAnR0VUJztcbiAgICBjb25zdCB1cmwgPSB0aGlzLkFQSV9VU0VSO1xuICAgIGNvbnN0IGJvZHkgPSBudWxsO1xuICAgIGNvbnN0IGh0dHBQYXJhbXMgPSBudWxsO1xuICAgIHJldHVybiB0aGlzLmh0dHBSZXF1ZXN0U2VydmljZS5odHRwUmVxdWVzdCh0eXBlLCB1cmwsIGJvZHksIGh0dHBQYXJhbXMpO1xuICB9XG5cbiAgZ2V0QXBpVXNlcigpIHtcbiAgICByZXR1cm4gdGhpcy5BUElfVVNFUjtcbiAgfVxuXG4gIHNldEFwaVVzZXIoQVBJX1VTRVI6IHN0cmluZykge1xuICAgIHRoaXMuQVBJX1VTRVIgPSBBUElfVVNFUjtcbiAgfVxufVxuIl0sIm5hbWVzIjpbIkluamVjdGFibGUiLCJOZ01vZHVsZSIsIk9ic2VydmFibGUiLCJIdHRwQ2xpZW50IiwiVXNlclNlcnZpY2UiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtRQU9FO1NBQWlCOztvQkFMbEJBLGFBQVUsU0FBQzt3QkFDVixVQUFVLEVBQUUsTUFBTTtxQkFDbkI7Ozs7OzhCQUpEOzs7Ozs7O0FDQUE7Ozs7b0JBRUNDLFdBQVEsU0FBQzt3QkFDUixPQUFPLEVBQUUsRUFDUjtxQkFDRjs7NkJBTEQ7Ozs7Ozs7QUNBQTtRQW1CRTswQkFaZ0I7Z0JBQ2QsS0FBSyxFQUFFO29CQUNMLElBQUksRUFBRSxJQUFJO29CQUNWLEtBQUssRUFBRSxJQUFJO2lCQUNaO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxRQUFRLEVBQUUsSUFBSTtvQkFDZCxTQUFTLEVBQUUsSUFBSTtvQkFDZixNQUFNLEVBQUUsSUFBSTtpQkFDYjthQUNGO1NBRWdCOztvQkFqQmxCRCxhQUFVLFNBQUM7d0JBQ1YsVUFBVSxFQUFFLE1BQU07cUJBQ25COzs7Ozs0QkFKRDs7Ozs7OztBQ0FBO1FBV0U7MkJBSmlCO2dCQUNmLFFBQVEsRUFBRSxJQUFJO2FBQ2Y7U0FFZ0I7O29CQVRsQkEsYUFBVSxTQUFDO3dCQUNWLFVBQVUsRUFBRSxNQUFNO3FCQUNuQjs7Ozs7NkJBSkQ7Ozs7Ozs7QUNDQTtRQWlCRSw0QkFDUyxlQUNBO1lBREEsa0JBQWEsR0FBYixhQUFhO1lBQ2IsbUJBQWMsR0FBZCxjQUFjO1NBQ3RCOzs7Ozs7Ozs7Ozs7UUFPRCxzQ0FBUzs7Ozs7O1lBQVQsVUFBVSxPQUF5QixFQUFFLElBQWlCO2dCQUF0RCxpQkF5QkM7O2dCQXhCQyxJQUFJLGFBQWEsQ0FBQzs7Z0JBQ2xCLElBQUksV0FBVyxDQUFDO2dCQUVoQixJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLEtBQUssSUFBSSxFQUFFO29CQUNsRCxhQUFhLEdBQUcsT0FBTyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDO29CQUN6RSxXQUFXLEdBQUcsbUNBQW1DLENBQUM7aUJBQ25EO3FCQUFNO29CQUNMLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO29CQUNuRyxXQUFXLEdBQUcsa0JBQWtCLENBQUM7aUJBQ2xDO2dCQUVELE9BQU8sR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDO29CQUN0QixVQUFVLEVBQUU7d0JBQ1YsZUFBZSxFQUFDLEVBQUUsR0FBRSxhQUFhO3dCQUNqQyxjQUFjLEVBQUMsRUFBRSxHQUFFLFdBQVc7d0JBQzlCLE9BQU8sRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxRQUFRLEtBQUssSUFBSSxHQUFHLEVBQUUsR0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsUUFBUTtxQkFDdEc7aUJBQ0YsQ0FBQyxDQUFDO2dCQUNILE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUM7cUJBQ3hCLEtBQUssQ0FBQyxVQUFBLEtBQUs7b0JBQ1Y7d0JBQ0UsT0FBTyxLQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUNuQztpQkFDRixDQUFDLENBQUM7YUFDTjs7Ozs7UUFFRCwyQ0FBYzs7OztZQUFkLFVBQWUsS0FBSztnQkFDbEIsSUFBSTtvQkFDRixPQUFPRSxlQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNoQztnQkFBQyxPQUFPLENBQUMsRUFBRTtvQkFDVixPQUFPQSxlQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNoQzthQUVGOztvQkFsREZGLGFBQVUsU0FBQzt3QkFDVixVQUFVLEVBQUUsTUFBTTtxQkFDbkI7Ozs7O3dCQUxRLGFBQWE7d0JBQ2IsY0FBYzs7OztpQ0FWdkI7Ozs7Ozs7QUNBQTtRQVNFLDRCQUFtQixVQUFzQjtZQUF0QixlQUFVLEdBQVYsVUFBVSxDQUFZO1NBQUs7Ozs7Ozs7O1FBRXZDLHdDQUFXOzs7Ozs7O3NCQUFDLFdBQW1CLEVBQUUsR0FBVyxFQUFFLFdBQWdCLEVBQUUsV0FBdUI7Z0JBRzVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLEdBQUcsRUFBRSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQzs7O29CQVZySEEsYUFBVSxTQUFDO3dCQUNWLFVBQVUsRUFBRSxNQUFNO3FCQUNuQjs7Ozs7d0JBTFFHLGFBQVU7Ozs7aUNBRG5COzs7Ozs7O0FDQUE7UUFZRSxzQkFBbUIsa0JBQXNDO1lBQXRDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7U0FBSzs7Ozs7UUFFOUQsNEJBQUs7Ozs7WUFBTCxVQUFNLFdBQWdCOztnQkFDcEIsSUFBTSxJQUFJLEdBQUcsTUFBTSxDQUFDOztnQkFDcEIsSUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQzs7Z0JBQzNCLElBQU0sSUFBSSxHQUFHLFdBQVcsQ0FBQzs7Z0JBQ3pCLElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQztnQkFDeEIsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFDO2FBQ3pFOzs7O1FBRUQsa0NBQVc7OztZQUFYO2dCQUNFLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQzthQUN2Qjs7Ozs7UUFFRCxrQ0FBVzs7OztZQUFYLFVBQVksU0FBaUI7Z0JBQzNCLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO2FBQzVCOztvQkF2QkZILGFBQVUsU0FBQzt3QkFDVixVQUFVLEVBQUUsTUFBTTtxQkFDbkI7Ozs7O3dCQU5RLGtCQUFrQjs7OzsyQkFEM0I7Ozs7Ozs7QUNBQTtRQVdFLHVCQUFtQixrQkFBc0M7WUFBdEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtTQUFLOzs7O1FBRTlELDhCQUFNOzs7WUFBTjs7Z0JBQ0UsSUFBTSxJQUFJLEdBQUcsTUFBTSxDQUFDOztnQkFDcEIsSUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQzs7Z0JBQzVCLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQzs7Z0JBQ2xCLElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQztnQkFDeEIsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFDO2FBQ3pFOzs7O1FBRUQsb0NBQVk7OztZQUFaO2dCQUNFLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQzthQUN4Qjs7Ozs7UUFFRCxvQ0FBWTs7OztZQUFaLFVBQWEsU0FBaUI7Z0JBQzVCLElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO2FBQzdCOztvQkF2QkZBLGFBQVUsU0FBQzt3QkFDVixVQUFVLEVBQUUsTUFBTTtxQkFDbkI7Ozs7O3dCQUxRLGtCQUFrQjs7Ozs0QkFEM0I7Ozs7Ozs7QUNBQTtRQWFFLHdCQUNTLG9CQUNBLGNBQ0E7WUFGQSx1QkFBa0IsR0FBbEIsa0JBQWtCO1lBQ2xCLGlCQUFZLEdBQVosWUFBWTtZQUNaLGtCQUFhLEdBQWIsYUFBYTtTQUNqQjs7OztRQUVMSSw4QkFBSzs7O1lBQUw7Z0JBRUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFFBQVEsR0FBRyxhQUFhLENBQUM7Z0JBQzNELElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsMENBQTBDLENBQUM7O2dCQUV0RixJQUFNLFdBQVcsR0FBUSxZQUFZLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFFBQVEsR0FBRyxpRUFBaUUsQ0FBQzs7Z0JBRXZKLElBQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQzs7Z0JBQ3BCLElBQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7O2dCQUMzQixJQUFNLElBQUksR0FBRyxXQUFXLENBQUM7O2dCQUN6QixJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUM7Z0JBQ3hCLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxVQUFVLENBQUMsQ0FBQzthQUN6RTs7Ozs7UUFFREEsbUNBQVU7Ozs7WUFBVixVQUFXLFFBQWE7O2dCQUN0QixJQUFNLElBQUksR0FBRyxLQUFLLENBQUM7O2dCQUNuQixJQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDOztnQkFDMUIsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDOztnQkFDbEIsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDO2dCQUN4QixPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUM7YUFDekU7Ozs7UUFFREEsbUNBQVU7OztZQUFWO2dCQUNFLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQzthQUN0Qjs7Ozs7UUFFREEsbUNBQVU7Ozs7WUFBVixVQUFXLFFBQWdCO2dCQUN6QixJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQzthQUMxQjs7b0JBMUNGSixhQUFVLFNBQUM7d0JBQ1YsVUFBVSxFQUFFLE1BQU07cUJBQ25COzs7Ozt3QkFOUSxrQkFBa0I7d0JBRWxCLFlBQVk7d0JBQUUsYUFBYTs7Ozs2QkFIcEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7In0=